const { createParkingLot, parKCar, status, leave } = require('./parkinglot')
const fs = require('fs')



const eachLineAction =  (command_value) => {
    let response = ""
    switch (command_value[0]) {
        case 'create_parking_lot':
            response = createParkingLot(command_value[1])
            break;
        case 'park':
            response = parKCar(command_value[1])
            break;
        case 'leave':
            response = leave(command_value[1], command_value[2])
            break;
        case 'status':
            status()
            break;
        default:
            console.log("Command Not Found")

    }

    return response

}


const init = async () => {

    //------- Read Input File ---------- //

    var contents = fs.readFileSync(__dirname + "/" + process.argv[2], 'utf8');
    commands = contents.split('\n')

    commands.forEach((eachcommand, index) => {
        const command_value = eachcommand.split(' ')
        const response = eachLineAction(command_value)
        console.log(response)

    })

    
}

const callInitOrNot = process.argv[2]
if (callInitOrNot) {
    init()
} else {
    console.log('Give me Input File')
}