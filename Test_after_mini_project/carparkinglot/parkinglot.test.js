const { createParkingLot, parKCar,status,leave }  = require('./parkinglot');

test('Creating parking Lot', () => {
    const output = "Created a parking lot with 6 slots";
    const response = createParkingLot(6)
    expect(output).toBe(response);
})


test('Parking car KA-01-HH-1234', () => {
    const output = "Allocated slot number: 1";
    const response =  parKCar("KA-01-HH-1234")
    expect(output).toBe(response);
})

test('Parking car KA-01-HH-1235', () => {
    const output = "Allocated slot number: 2";
    const response =  parKCar("KA-01-HH-1235")
    expect(output).toBe(response);
})

test('Parking car KA-01-HH-1236', () => {
    const output = "Allocated slot number: 3";
    const response =  parKCar("KA-01-HH-1236")
    expect(output).toBe(response);
})

test('Parking car KA-01-HH-9999', () => {
    const output = "Allocated slot number: 4";
    const response =  parKCar("KA-01-HH-9999")
    expect(output).toBe(response);
})

test('Parking car KA-01-HH-8765', () => {
    const output = "Allocated slot number: 5";
    const response =  parKCar("KA-01-HH-8765")
    expect(output).toBe(response);
})

test('Parking car KA-01-HH-0987', () => {
    const output = "Allocated slot number: 6";
    const response =  parKCar("KA-01-HH-0987")
    expect(output).toBe(response);
})

test('Car Number KA-01-HH-1234 is leaving and in slot for 4 hours', () => {
    const output = `Register Number KA-01-HH-1234 with Slot Number 1 is free with Charge 30`
    const response =  leave("KA-01-HH-1234", 4)
    expect(output).toBe(response);
})

test('Checking the status of Parking lot', () => {
    const response =  status()
    expect(5).toBe(response);
});

test('Parking car KA-01-HH-1234', () => {
    const output = "Allocated slot number: 1";
    const response =  parKCar("KA-01-HH-1234")
    expect(output).toBe(response);
});

test('Parking car KA-01-HH-8888', () => {
    const output = "Sorry, parking lot is full";
    const response =  parKCar("KA-01-HH-8888")
    expect(output).toBe(response);
});

test('Car Number KA-01-HH-1234 is leaving and in slot for 4 hours', () => {
    const output = `Register Number KA-01-HH-1234 with Slot Number 1 is free with Charge 30`
    const response =  leave("KA-01-HH-1234", 4)
    expect(output).toBe(response);
})



test('Car Number KA-01-HH-8888 is leaving and in slot for 2 hours', () => {
    const output = `Register Number KA-01-HH-8888 not found`
    const response =  leave("KA-01-HH-8888", 2)
    expect(output).toBe(response);
})

test('Parking car KA-01-HH-3333', () => {
    const output = "Allocated slot number: 1";
    const response =  parKCar("KA-01-HH-3333")
    expect(output).toBe(response);
})


test('Parking car KA-01-HH-1111', () => {
    const output = "Sorry, parking lot is full";
    const response =  parKCar("KA-01-HH-1111")
    expect(output).toBe(response);
})

test('Checking the status of Parking lot', () => {
    const response =  status()
    expect(6).toBe(response);
})

test('Parking car KA-01-HH-33338', () => {
    const output = "Sorry, parking lot is full";
    const response =  parKCar("KA-01-HH-33338")
    expect(output).toBe(response);
})




    
   
    



