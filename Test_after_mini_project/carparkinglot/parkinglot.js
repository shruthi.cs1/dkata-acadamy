let ParkedCar = [];
const Slots = [];
let maxSlots = 0
const basic_price = 10

const createParkingLot = (capacity) => {

    if (parseInt(capacity) > 0) {
        maxSlots = parseInt(capacity)
        Array(maxSlots).fill().map((_, i) => {
            const slot = {
                "slot_number": i + 1,
                "available": true
            }
            Slots.push(slot)
        });
    }
    return `Created a parking lot with ${Slots.length} slots`;
}

const get_slot_index = (value, key) => {
    let slot_index = Slots.findIndex(eachslot => eachslot[key] == value)
    return slot_index
}

const parKCar =  (car_number) => {
    if (maxSlots === ParkedCar.length) {
        return `Sorry, parking lot is full`;
    }
    let slot_index = get_slot_index(true, "available")
    const carData = {
        "slot_number": slot_index + 1,
        "car_number": car_number,
        "parked_at": new Date(),
        "charge": basic_price
    }
    ParkedCar.push(carData);
    Slots[slot_index]["available"] = false
    return `Allocated slot number: ${carData.slot_number}`

}

const removecar = (car_number) => {
    ParkedCar = ParkedCar.filter(eachcar => eachcar.car_number != car_number)
    return 0

}



const leave =  (car_number, hours) => {
    let car_slot_index = ParkedCar.findIndex(eachcar => eachcar.car_number == car_number)
    let response = `Register Number ${car_number} not found`
    if (car_slot_index != -1) {
        const carData = ParkedCar[car_slot_index]
        const slot_index = get_slot_index(carData.slot_number, 'slot_number')
        Slots[slot_index]["available"] = true
        const removedcar = removecar(car_number)
        const totalCharge = hours > 2 ? carData.charge + (hours - 2) * 10 : carData.charge
        response = `Register Number ${car_number} with Slot Number ${carData.slot_number} is free with Charge ${totalCharge}`
    }
    return response

}

const status =  () => {
    console.log("Slot No. Registration N0.")
    ParkedCar.forEach(eachcar => console.log(`${eachcar.slot_number} ${eachcar.car_number}`))
    return ParkedCar.length
}



module.exports = { createParkingLot, parKCar, status, leave }