
const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { init } = require('../server');

describe('GET /', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
    });

    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/'
        });
        expect(res.statusCode).to.equal(404);
    });

    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/v1/pets'
        });
        expect(res.statusCode).to.equal(200);
    });

    it('responds with 200', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/v1/pets/4'
        });

        let output = res.result.reduce((out,eachpet) => { return eachpet})
        expect(res.statusCode).to.equal(200);
        expect(output.id).to.equal(4)
        expect(res.result.length).to.equal(1)

    });
});