const capitilize=require('./server')
const fs=require('fs')

test('Checking if file is not exist',()=>{
    if(fs.existsSync('output.txt')) {
        fs.unlinkSync('output.txt')
    }
    expect(fs.existsSync('output.txt')).toBeFalsy()
})

test('Capitilizing the input file content',()=>{
    capitilize('input.txt','output.txt')
    expect(fs.existsSync('output.txt')).toBeTruthy()
})
