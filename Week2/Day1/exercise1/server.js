const fs=require('fs')
const {Transform}=require('stream')

const inputFile=process.argv[2]
const outputFile=process.argv[3]

String.prototype.capitalize=function(){
    return this.replace(/\s*\w/, x=>x.toUpperCase()).replace(/\.\s*\w/g,x=>x.toUpperCase())
}

const capitilize=(inputarg,outputarg)=>{
    const uncapitalizedData=inputFile?inputFile:inputarg
    const capitilizedData=outputFile?outputFile:outputarg

    const readStream=fs.createReadStream(uncapitalizedData)
    const writeStream=fs.createWriteStream(capitilizedData)

    const CapitalizeTransform=new Transform({
        transform(chunck,enc,done){
            this.push(chunck.toString().capitalize())
            done()
        }
    })
    readStream.pipe(CapitalizeTransform).pipe(writeStream)
}

module.exports=capitilize
if(inputFile){
    capitilize()
    fs.readFile(inputFile,'utf8',(err,data)=>console.log(data))
}
