const glob=require('glob')
const inputPath=process.argv[2]
const getImageFiles=()=>{
    const path=inputPath?inputPath:"."
    return glob.sync(`${path}/**/*.{jpg,png,jpeg}`,{})
}

module.exports=getImageFiles

if(inputPath){
    console.log(getImageFiles())
}else{
    console.log("No Argument passed")
}
