const getImageFiles=require('./server')

test('Find Image Files',()=>{
    expect(getImageFiles()).toEqual(
        [
            './banner2.jpg',
            './banner3.jpg',
            './FB-cover-for-friday-memes.jpg',
            './flyer1.jpg',
            './flyer2.jpg',
            './hiclipart.com.png',
            './offercard5.jpg'
          ]
    )
})


