const person=require('./person')
const shruthi=new person.Trainee('shruthi','cs',27,'Female','Developer','Bangalore','Singing')
const radha=new person.Trainee('radha','reddy',37,'male','Team Lead','Bangalore','Talking')

const trainees=[shruthi,radha]

const getTime=time=>{
    const convertedTime=new Date(time)
    return {
        hour:convertedTime.getHours(),
        minute:convertedTime.getMinutes(),
        second:convertedTime.getSeconds(),
    }
}

const rootHandler=(request,h)=>{
    return 'This is a root route'
}

const nameHandler=(request,h)=>{
    return `Hello ${request.params.name}`
}

const traineesHandler=(request,h)=>{
    return trainees
}

const parseTimeHandler=(request,h)=>{
    return getTime(request.query.iso)
}

module.exports={rootHandler,nameHandler,traineesHandler,parseTimeHandler,trainees}
