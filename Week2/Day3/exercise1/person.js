class Person{
    constructor(first,last,age,gender,job,address){
        this.name=first+' '+last
        this.age=age
        this.gender=gender
        this.job=job
        this.address=address
    }
    greeting(){
        return `Hi, I am ${this.name}, from ${this.address}`
    }
}

class Trainer extends Person{
    constructor(first,last,age,gender,job,address,specialist,certificate){
        super(first,last,age,gender,job,address)
        this.specialist=specialist
        this.certificate=certificate
    }
    greeting(){
        return `${super.greeting()}, my specialize is ${this.specialist}`
    }
}

class Trainee extends Person{
    constructor(first,last,age,gender,job,address,interest){
        super(first,last,age,gender,job,address)
        this.interest=interest
        
    }
    greeting(){
        return `${super.greeting()}, my interest is ${this.interest}`
    }
}

module.exports={Trainee,Trainer}
