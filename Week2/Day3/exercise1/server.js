const Hapi=require('@hapi/hapi')
const Joi=require('@hapi/joi')
const Inert=require('@hapi/inert')
const {rootHandler,nameHandler,parseTimeHandler,traineesHandler}=require('./handler')

const start=async()=>{
    const server=Hapi.server({
        port:8000,
        host:'localhost'
    })
    
    await server.register(Inert)
        
    server.route({
        method:'GET',
        path:'/',
        handler:rootHandler
    })
    
    server.route({
        method:'GET',
        path:'/api/v1/hello/{name}',
        handler:nameHandler,
        options:{
            validate:{
                params:{
                    name:Joi.string().regex(/^[A-Za-z]{3,}$/)
                }
            }
        }
    })
    
    server.route({
        method:'GET',
        path:'/api/v1/trainees',
        handler:traineesHandler
    })
    
    server.route({
        method:'GET',
        path:'/api/v1/parsetime',
        handler:parseTimeHandler
    })
    
    server.route({
        method:'GET',
        path:'/api/v1/images/{file*}',
        handler:{
            directory:{
                path:'./images',
                listing:true
            }
        },
        options:{
            validate:{
                params:{
                    file:Joi.string().regex(/(^[^\.]*$)|(\.(png)|(jpg)|(jpeg){1}$)/)
                }
            }
        }
    })
    
    await server.start()
    
    console.log(`Server running on ${server.info.uri}`)
    
    process.on('unhandledRejection',(err)=>{
        console.log(err)
        process.exit(1)
    })
    
    return server
}

module.exports={start}

const runOrNot=process.argv[2]
if(runOrNot=='run'){
    start()
}
