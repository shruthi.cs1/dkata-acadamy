Day 1 


Basics of Node 
Why Node?
Default Node Modules 
How to use glob
How to use readline
What is Stream? How does the stream work?
What is the pipe? and How to use Pipe in node?



Day 2

Third-party package: Axios
What is JSON Data? 
what is the difference between javascript object and JSON?
Read request params 
How to create a server using an HTTP module? 
Date parse 

Day 3 

Create a Project for Hapi JS.
Installing Hapi JS.
Create Server.
Basic Routing.
Lifecycle
Serve Static Files.
Why hapi. What does it provide over and above the plain node

Day 4

Creating restful APIs using Hapi and Testing routers 
Create, Read, Update and delete APIs 
With respective validation 
Testing the response result or payload 
HTTP status code and HTTP verbs 



