let server=require('./server')
const fs=require('fs')
const axios=require('../exercise2/node_modules/axios')

beforeAll(()=>{
    server.listen(1126)
})

afterAll(()=>{
    server.close()
})

test(`To check Base Route`,async ()=>{
    const apiUrl='http://localhost:1126/'
    await axios.get(apiUrl)
        .catch(err=>{
            expect(err.response.data.message).toBe('This is a root route')
        })
})

test(`Response status code for path '/api/v1/name/radha' is 200`,async ()=>{
    const apiUrl='http://localhost:1126/api/v1/name/radha'
    await axios.get(apiUrl)
        .then(response=>{
            expect(response.status).toBe(200)
        })
})

test(`Response result for path '/api/v1/name/radha' is 'Hi, i am Radha'`,async ()=>{
    const apiUrl='http://localhost:1126/api/v1/name/radha'
    await axios.get(apiUrl)
        .then(response=>{
            expect(response.data).toBe('Hi, i am radha')
        })
})

test(`Response status code for path '/api/v1/trainees' is 200`,async ()=>{
    const apiUrl='http://localhost:1126/api/v1/trainees'
    await axios.get(apiUrl)
        .then(response=>{
            expect(response.status).toBe(200)
        })
})



test(`Response status code for path '/api/v1/parsetime' is 400`,async ()=>{
    const apiUrl='http://localhost:1126/api/v1/parsetime'
    await axios.get(apiUrl)
        .catch(err=>{
            expect(err.response.status).toBe(400)
        })
})

test(`Response status code for path '/api/v1/parsetime?time=1' is 400`,async ()=>{
    const apiUrl='http://localhost:1126/api/v1/parsetime?time=1'
    await axios.get(apiUrl)
        .catch(err=>{
            expect(err.response.status).toBe(400)
        })
})

test(`Response status code for path '/api/v1/parsetime?random=9:34 PM 11/29/2019' is 400`,async ()=>{
    const apiUrl='http://localhost:1126/api/v1/parsetime?random=9:34 PM 11/29/2019'
    await axios.get(apiUrl)
        .catch(err=>{
            expect(err.response.status).toBe(400)
        })
})

test(`Response status code for path '/api/v1/parsetime?time=9:34 PM 11/29/2019' is 200`,async ()=>{
    const apiUrl='http://localhost:1126/api/v1/parsetime?time=9:34 PM 11/29/2019'
    await axios.get(apiUrl)
        .then(response=>{
            expect(response.status).toBe(200)
        })
})

test(`Response result for path '/api/v1/parsetime?time=9:34 PM 11/29/2019' is object with property hour,minute,second`,async ()=>{
    const apiUrl='http://localhost:1126/api/v1/parsetime?time=9:34 PM 11/29/2019'
    await axios.get(apiUrl)
        .then(response=>{
            expect(response.data).toEqual({
                hour:21,
                minute:34,
                second:0
            })
        })
})
