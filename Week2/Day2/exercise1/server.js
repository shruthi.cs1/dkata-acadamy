const http=require('http')
const url=require('url')
const {trainees,getTime}=require('./handler')

const server=http.createServer((req,res)=>{
    if(req.method=='GET') {
        const uri=url.parse(req.url,true)
        const path=uri.pathname
        let output
        if(path=='/') {
            output='This is a root route'
        } else if(path.includes('/api/v1/name/')){
            let pathValues = path.split('/')
            output='Hi, i am '+pathValues[pathValues.length -1]
        } else if(path=='/api/v1/parsetime'){
            if(new Date(uri.query.time)=='Invalid Date') {
                res.writeHead(400,{'Content-Type':'application/json'})
                return res.end(JSON.stringify({
                    statusCode:400,
                    error:'Invalid Request',
                    message:'Invalid Request Query Input'
                }))
            }
            output=getTime(uri.query.time)
        } else if(path=='/api/v1/trainees'){
            output=trainees
        } else {
            let output=`no route for path ${path}`
            res.writeHead(404,{'Content-Type':'application/json'})
            return res.end(JSON.stringify({
                statusCode:404,
                error:"Not Found",
                message:output
            }))
        }
        res.writeHead(200,{'Content-Type':'application/json'})
        return res.end(output)
    }
})

module.exports=server

const port=process.argv[2]
if(port){
    server.listen(port)
    console.log(`Server running on port ${port}`)
}
