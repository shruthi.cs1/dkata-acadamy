const person=require('./person')
const shruthi=new person.Trainee('shruthi','cs',27,'Female','Developer','Bangalore','Singing')
const radha=new person.Trainee('radha','reddy',37,'male','Team Lead','Bangalore','Talking')

const trainees=JSON.stringify([shruthi,radha])

const getTime=time=>{
    const convertedTime=new Date(time)
    return JSON.stringify({
        hour: convertedTime.getHours(),
        minute: convertedTime.getMinutes(),
        second: convertedTime.getSeconds()
    })
}

module.exports={trainees,getTime}
