let server=require('./server')
const fs=require('fs')
const axios=require('../exercise1/node_modules/axios')
const downloadImage=require('./downloadImage')



beforeAll(()=>{
    //server.listen(1126)
})

afterAll(()=>{
    server.close()
})

test('Download image',async ()=>{
    const apiUrl='https://xkcd.com/info.0.json'
    axios.get(apiUrl)
        .then(response=>downloadImage(response))
        .then(imageName=>{
            expect(fs.existsSync(`comicsImages/${imageName}`)).toBeTruthy()
    })
})
