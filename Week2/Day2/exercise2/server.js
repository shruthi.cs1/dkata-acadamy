const downloadImage=require('./downloadImage')
const port=8000
const axios=require('../exercise1/node_modules/axios')
const apiUrl='https://xkcd.com/info.0.json'
const http=require('http')
const url=require('url')

const server=http.createServer((req,res)=>{
    if(req.method=='GET') {
        const uri=url.parse(req.url,true)
        const path=uri.pathname
        if(path=='/') {
            axios.get(apiUrl)
            .then(response=>{
                res.writeHead(200,{'Content-Type':'application/json'})
                downloadImage(response)
                return JSON.stringify(response.dada)
            })
        } 
        
    }
})

module.exports=server

if(port){
    server.listen(port)
    console.log(`Server running on port ${port}`)
}
