const fs=require('fs')
const path=require('path')
const axios=require('../exercise1/node_modules/axios')

const downloadImage=async response=>{
    const url=response.data.img
    const imageName=path.basename(url)
    const imagePath=`comicsImages/${imageName}`
    const writeStream=fs.createWriteStream(imagePath)
    const rs=await axios({url,method:'GET',responseType:'stream'})
    rs.data.pipe(writeStream)
    return imageName
}

module.exports=downloadImage

const urlApi=process.argv[2]
if(urlApi){
    axios.get(urlApi)
    .then(response=>downloadImage(response))
}
