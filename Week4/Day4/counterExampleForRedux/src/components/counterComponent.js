import React from 'react';
import { Text, View,Button  } from 'react-native';
import {styles} from '../styles/rootStyle'
import { connect } from "react-redux";
import {IncrementNumber,DecrementNumber} from '../action'




class Counter extends React.Component {

  constructor(props){
    super(props);
  }



  render(){
    
    return(
      <View style={styles.mainContainer}>
        <View style={styles.container}>
          <Text>My Counter Vlaue {this.props.counter.value}</Text>
        </View>
        <View style={styles.buttonContainer}>
        <Button
          title="Press to increment"
          onPress={() => this.props.IncrementNumber(5)}
        />
          <Button
          title="Press to decrement"
          onPress={() => this.props.DecrementNumber(5)}
        />
       
        </View>
        
      </View>
    );
  }
}

const mapStateToProps = (state) => {
	return {
		counter: state.counter
	}
};

export default connect(mapStateToProps, {IncrementNumber,DecrementNumber})(Counter);