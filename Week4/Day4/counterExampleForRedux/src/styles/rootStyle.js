import { StyleSheet } from 'react-native';
exports.styles = StyleSheet.create({
    container: {flex:1,alignItems:'center',backgroundColor:'#ce3914'},
    mainContainer : {margin:20,flexDirection:'column'},
    buttonContainer : {flexDirection:'row',margin:20,justifyContent:'center',paddingTop:30}
  })