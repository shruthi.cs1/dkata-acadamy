export const increment = (value) => {
    return {
        type : 'INCREMENT',
        payload: value
    }
    
}

export const decrement = (value) => {
    return {
        type : 'DECREMENT',
        payload: value
    }
}

export const IncrementNumber = (number) => {
    return(dispatch) => {
        dispatch(increment(number))
    }
}

export const DecrementNumber = (number) => {
    return(dispatch) => {
        dispatch(decrement(number))
    }
}

