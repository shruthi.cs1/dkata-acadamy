import React from 'react'
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import PetsContainer from "./components/petsContainer"
import PetDetail from "./components/petDetailComponent"
import AddPet from "./components/addPetComponent"
import store from './store'
import { Provider } from 'react-redux'
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';


const MainNavigator = createStackNavigator({
  PetsList: { screen: PetsContainer },
  PetDetail: { screen: PetDetail },
  AddPet: { screen: AddPet }
});

const Navigation = createAppContainer(MainNavigator);

// Render the app container component with the provider around it
export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <PaperProvider>
          <Navigation />
        </PaperProvider>
      </Provider>
    );
  }
}