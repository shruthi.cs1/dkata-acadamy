import rootReducer from './reducer'
import { applyMiddleware, createStore, compose } from "redux";
import { createLogger } from "redux-logger";
import thunk from "redux-thunk";




const middlerwares = [thunk];
if (__DEV__) {
	middlerwares.push(createLogger())
}
const store = createStore(
	rootReducer,
	compose(
		applyMiddleware(...middlerwares)
	)
);


export default store;