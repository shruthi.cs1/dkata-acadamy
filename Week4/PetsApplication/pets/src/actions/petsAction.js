import { config } from "../config"
import Axios from 'axios'
import path from 'path'

export const gettingPetSuccess = (pets) => {
    return {
        type: 'GETTINGPETSUCCESS',
        pets: pets
    }
}

export const gettingPetFailed = (error) => {
    return {
        type: 'GETTINGPETFAILED',
        error: error
    }
}

export const addedPetSuccess = (pet) => {
    return {
        type: 'ADDEDPETSUCCESS',
        addedpet: pet
    }
}

export const addedPetFailed = (error) => {
    return {
        type: 'ADDEDPETFAILED',
        error: error
    }
}

export const updatePetSuccess = (pet) => {
    return {
        type: 'UPDATEDPETSUCESS',
        updatedpet: pet
    }
}

export const updatePetFailed = (error) => {
    return {
        type: 'UPDATEDPETFAILED',
        error: error
    }
}

export const uploadImageSuccess = (imagePath) => {
    return {
        type: 'UPLOADIMAGESUCESS',
        imagePath: imagePath
    }
}

export const uploadImageFailure = (error) => {
    return {
        type: 'UPLOADIMAGEFAILED'
    }
}

export const getPets = () => {
    return (dispatch) => {
        return Axios.get(`${config.apiURL}/pets?sort=id&limit=20&offset=0`)
            .then((responseJson) => {
                dispatch(gettingPetSuccess(responseJson.data))
            })
            .catch((error) => {
                dispatch(gettingPetFailed("Error while getting Pets"))
            });

    }


}

export const addPet = (petPayload) => {
    // Call Api 
    return (dispatch) => {
        return Axios.post(`${config.apiURL}/pets`, petPayload)
            .then((responseJson) => {
                dispatch(addedPetSuccess(responseJson.data))
            })
            .catch((error) => {
                dispatch(addedPetFailed(error))
            });

    }

}

export const updatePet = (petPayload, id) => {
    // Call Api 
    return (dispatch) => {
        return Axios.put(`${config.apiURL}/pets/${id}`, petPayload)
            .then((responseJson) => {
                dispatch(updatePetSuccess(responseJson.data))
            })
            .catch((error) => {
                dispatch(updatePetFailed(error))
            });

    }

}

export const uploadImage = (file) => {
    let form = new FormData()
    const fileDetail = path.parse(file)
    form.append(`file`, {
        uri: file,
        name: fileDetail.base,
        type: `image/${fileDetail.ext.slice(1)}`
    })
    return dispatch => {
        return Axios.post(`${config.apiURL}/upload`, form)
            .then(res => {
                dispatch(uploadImageSuccess(res.data))
                return Promise.resolve(res.data)
            })
            .catch(err => {
                dispatch(uploadImageFailure())

            })
    }
}


