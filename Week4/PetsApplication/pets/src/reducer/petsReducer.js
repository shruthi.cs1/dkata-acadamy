const INITIAL_STATE = { petList: [], error: "", isLoading: true, imagePath: "" };


export const pets = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'GETTINGPETSUCCESS':
            return { ...state, isLoading: false, petList: action.pets }
        case 'GETTINGPETFAILED':
            return { ...state, isLoading: false, error: action.error }
        case 'ADDEDPETSUCCESS':
            return { ...state, isLoading: false, petList: [...state.petList, ...action.addedpet] }
        case 'ADDEDPETFAILED':
            return { ...state, error: action.error }
        case 'UPDATEDPETSUCESS':
            const index = state.petList.findIndex(eachpet => eachpet.id === action.updatedpet.id)
            let petList = state.petList
            petList[index] = action.updatedpet
            return { ...state, isLoading: false, petList: petList }
        case 'UPDATEDPETFAILED':
            return { ...state, isLoading: false, error: action.error }
        case 'UPLOADIMAGESUCESS':
            return { ...state, imagePath: action.imagePath }
        default:
            return state

    }

}