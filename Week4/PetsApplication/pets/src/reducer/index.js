import {combineReducers} from 'redux'
import { pets } from './petsReducer'

const appReducer = combineReducers({
    pets:pets
})

const rootReducer = (state, action) => {
	return appReducer(state, action);
};

export default rootReducer;