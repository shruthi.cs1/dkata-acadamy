import React from 'react';
import { FlatList, ActivityIndicator, Text, View } from 'react-native';
import { Pet } from './petComponent'
import { styles } from '../styles/AppStyles'
import { connect } from 'react-redux';
import { getPets } from "../actions/petsAction"
import { FAB } from 'react-native-paper';


class PetsContainer extends React.Component {
  static navigationOptions = {
    title: 'My Pets',
    headerStyle: {
      backgroundColor: '#f4511e',
    },
    headerTintColor: 'blue',
    headerTitleStyle: {
      fontWeight: 'bold',

    },
  };

  constructor(props) {
    super(props);
    this.state = { isLoading: true, dataSource: [] }
  }

  componentDidMount() {
    this.props.getPets()
  }



  render() {
    const { navigate } = this.props.navigation;

    if (this.props.pets.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <Text>Getting Pets List...</Text>
          <ActivityIndicator />
        </View>
      )
    } else if (this.props.pets.petList.length <= 0) {

      return (<View style={{ flex: 1, padding: 20 }}>
        <Text>Add A First Pet ...</Text>
        <FAB
          style={styles.fab}
          medium
          label="Add Pet"
          onPress={() => {
            this.props.navigation.navigate('AddPet')
          }}
        />

      </View>)
    }

    return (
      <View style={{ flex: 1 }}>

        <View style={styles.container}>
          <FlatList
            data={this.props.pets.petList}
            renderItem={({ item }) => Pet(item, navigate)}
            keyExtractor={({ id }) => id}
          />
        </View>

        <FAB
          style={styles.fab}
          medium
          label="Add Pet"
          onPress={() => {
            this.props.navigation.navigate('AddPet')
          }}
        />


      </View>
    );
  }
}

const mapStateTpProps = (state) => {
  return {
    pets: state.pets
  }
}

export default connect(mapStateTpProps, { getPets, })(PetsContainer)
