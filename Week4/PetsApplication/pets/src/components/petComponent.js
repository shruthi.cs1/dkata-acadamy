import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { styles } from '../styles/AppStyles'
import { Avatar, Card } from 'react-native-paper';
import { config } from "../config"


exports.Pet = (props, navigate) => {

    return (
        <TouchableOpacity onPress={() => navigate('PetDetail', { petdetail: props })}>
            <Card>
                <Card.Title title={props.name} subtitle={props.breed} left={(props) => <Avatar.Image size={44} source={{ uri: "https://www.guidedogs.com.au/sites/default/files/styles/content_listing_full/public/node/news/image/guide_dog_puppy_in_a_harness.jpg?itok=-GDkDb4R" }} />} />
                <Card.Content>
                    <View>
                        <Card.Cover resizeMode={'contain'} source={{ uri: props.image_url ? `${config.apiURL}/profile/${props.image_url}` : "https://www.guidedogs.com.au/sites/default/files/styles/content_listing_full/public/node/news/image/guide_dog_puppy_in_a_harness.jpg?itok=-GDkDb4R" }} />
                    </View>
                    <View style={styles.petInformation}>
                        <Text style={styles.leftContainer}>Age: {props.age} years old</Text>
                        <Text style={styles.rightContainer}>Colour : {props.colour}</Text>
                    </View>
                </Card.Content>
            </Card>
        </TouchableOpacity>
    );
}

