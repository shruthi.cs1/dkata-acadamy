import React from 'react';
import { View,ScrollView,Text,Alert,ToastAndroid} from 'react-native';
import {styles} from '../styles/AppStyles'
import { TextInput, Card,Button,Chip} from 'react-native-paper';
import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker';
import TagInput from 'react-native-tags-input';
import { updatePet,uploadImage } from "../actions/petsAction"
import { connect } from 'react-redux';
import { config } from "../config"


class PetDetail extends React.Component {
  static navigationOptions = {
    title: 'Pet Detail',
    
  };
  constructor(props){
    super(props);
    this.state ={ isLoading:true,dataSource:[],petdetail:{},uri:null,valueChanged:false,tags: {
      tag: '',
      tagsArray: []
    }}
  }

  componentDidMount(){
      let tags = {...this.state.tags}
      tags.tagsArray = this.props.navigation.state.params.petdetail.vaccinations
      const petdetail = this.props.navigation.state.params.petdetail
      petdetail.image_url = `${config.apiURL}/profile/${this.props.navigation.state.params.petdetail.image_url}`
      this.setState({petdetail,tags})
  }

  componentWillUnmount(){

    if(this.state.valueChanged)
    {
     
      const updatePayload = {
      age:parseInt(this.state.petdetail.age),
      next_checkup:this.state.petdetail.next_checkup,
      vaccinations:this.state.petdetail.vaccinations
      }

      if(this.state.uri){
      this.props.uploadImage(this.state.petdetail.image_url).then((imagePath)=>{
        updatePayload["image_url"] = imagePath
        this.props.updatePet(updatePayload,this.state.petdetail.id).then(()=>{
          ToastAndroid.show("Updated Successfully", ToastAndroid.SHORT);
        })
      })
     }else{
      
      this.props.updatePet(updatePayload,this.state.petdetail.id).then(()=>{
        ToastAndroid.show("Updated Successfully", ToastAndroid.SHORT);

      })
     }
    }
    
  }

  changePicture = () => {
    const options = {
      title: 'Select Pet Image',
      customButtons: [],
      maxWidth:500,
      maxHeight:300,
      storageOptions: {
        skipBackup: true,
        path: 'images',

      },
    };
    
    /**
     * The first arg is the options object for customization (it can also be null or omitted for default options),
     * The second arg is the callback which sends object: response (more info in the API Reference)
     */
    ImagePicker.showImagePicker(options, (response) => {    
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };
    
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
        let petdetail = {...this.state.petdetail}
        petdetail.image_url = response.uri;
        this.setState({petdetail,uri:response.uri,valueChanged:true})
      }
    });
  }

  
  updateTagState = (state) => {
    let petdetail = {...this.state.petdetail}
    petdetail.vaccinations = state.tagsArray;
    this.setState({
      tags: state,
      petdetail,
      valueChanged:true
    })
  };

  render(){

    const {petdetail} = this.state
    

    return(
      <ScrollView>
        <Card>
            <Card.Content>
            <View>
                <Card.Cover resizeMode={'contain'} source={{ uri: petdetail.image_url ? petdetail.image_url :"https://www.guidedogs.com.au/sites/default/files/styles/content_listing_full/public/node/news/image/guide_dog_puppy_in_a_harness.jpg?itok=-GDkDb4R" }} />
            </View>
            <Card.Actions>
                <Button onPress={this.changePicture}>Change Picture</Button>
            </Card.Actions>

            </Card.Content>
      </Card>
      <Card>
        <Card.Content >
            <TextInput
            label='Name'
            Type="outlined"
            value={petdetail.name}
            editable={false}
          />
           <TextInput
            label='Breed'
            style={{marginTop:10}}
            value={petdetail.breed}
            editable={false}
          />
          <TextInput
            label='Color'
            style={{marginTop:10}}
            value={petdetail.colour}
            editable={false}
          />
           <TextInput
            label='Age'
            keyboardType={'numeric'}
            style={{marginTop:10}}
            value={ petdetail.age && petdetail.age.toString()}
            onChangeText={text => {
              let petdetail = {...this.state.petdetail}
              petdetail.age = text;
              this.setState({petdetail,valueChanged:true})
            }}
          />

          <Text style={{marginTop:20}}>Next Checkup Date</Text>
          <DatePicker
          style={{width: 200,marginTop:10}}
          date={petdetail.next_checkup} //initial date from state
          mode="date" //The enum of date, datetime and time
          placeholder="select date"
          format="YYYY-MM-DD"
          minDate={new Date()}
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          customStyles={{
            dateIcon: {
              position: 'absolute',
              left: 0,
              top: 4,
              marginLeft: 0
            },
            dateInput: {
              marginLeft: 36
            }
          }}
          onDateChange={(date) => {
            let petdetail = {...this.state.petdetail}
              petdetail.next_checkup = date;
              this.setState({petdetail,valueChanged:true})
          }}
        />
          <Text style={{marginTop:20}}>Pet's vaccinations</Text>
          <View style={{flex:1,flexDirection:'row',flexWrap:'wrap'}}>
              
          <TagInput
              updateState={this.updateTagState}
              tags={this.state.tags}
              tagStyle={styles.tag}
              tagTextStyle={styles.tagText}
              inputContainerStyle={[styles.textInput]}
              placeholder="Add Vaccinations here.."  
              keysForTag={', '} 

          /> 
          </View>
          
        </Card.Content>
      </Card>
    </ScrollView>

    );
  }
}

const mapStateToProps = (state) => {
  return {
    pets: state.pets
  }

}

export default connect(mapStateToProps,{updatePet,uploadImage})(PetDetail)

