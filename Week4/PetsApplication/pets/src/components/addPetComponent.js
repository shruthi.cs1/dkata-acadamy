import React from 'react';
import { View, ScrollView, Text, Alert } from 'react-native';
import { styles } from '../styles/AppStyles'
import { TextInput, Card, Button, Chip } from 'react-native-paper';
import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker';
import TagInput from 'react-native-tags-input';
import { addPet, uploadImage } from "../actions/petsAction"
import { connect } from 'react-redux';

class AddPet extends React.Component {

  static navigationOptions = {
    title: 'Add Pet'
  };

  constructor(props) {
    super(props);
    this.state = {
      uri: "", isLoading: true, dataSource: [], petdetail: {
        "name": null,
        "breed": "",
        "colour": "",
        "age": "",
        "next_checkup": new Date(),
        "vaccinations": [],
        "image_url": null
      }, valueChanged: false, tags: {
        tag: '',
        tagsArray: []
      }
    }
  }

  componentWillUnmount() {

    if (!this.state.petdetail.name && this.state.valueChanged) {
      Alert.alert(
        'Pet Information is Not Saved',
        'My Pet Name Cannot be empty',
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ],
        { cancelable: false },
      );
    } else {
      if (this.state.uri) {
        this.props.uploadImage(this.state.petdetail.image_url).then((imagePath) => {
          this.state.petdetail.image_url = imagePath
          this.props.addPet(this.state.petdetail)
        })
      }
    }


  }

  changePicture = () => {
    const options = {
      title: 'Select Pet Image',
      customButtons: [],
      maxWidth: 500,
      maxHeight: 300,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    /**
     * The first arg is the options object for customization (it can also be null or omitted for default options),
     * The second arg is the callback which sends object: response (more info in the API Reference)
     */
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
        let petdetail = { ...this.state.petdetail }
        petdetail.image_url = response.uri;
        this.setState({ petdetail, valueChanged: true, uri: response.uri })
      }
    });
  }

  updateTagState = (state) => {
    let petdetail = { ...this.state.petdetail }
    petdetail.vaccinations = state.tagsArray;
    this.setState({ petdetail, valueChanged: true })
    this.setState({
      tags: state,
      valueChanged: true,

    })
  };

  render() {

    const { petdetail } = this.state



    return (
      <ScrollView>
        <Card>
          <Card.Content>
            {petdetail.image_url &&
              <Card.Cover resizeMode={'contain'} source={{ uri: petdetail.image_url ? petdetail.image_url : "https://www.guidedogs.com.au/sites/default/files/styles/content_listing_full/public/node/news/image/guide_dog_puppy_in_a_harness.jpg?itok=-GDkDb4R" }} />
            }
            <Card.Actions>
              <Button onPress={this.changePicture}>{petdetail.image_url ? "Change Picture" : "Add Picture"}</Button>
            </Card.Actions>

          </Card.Content>
        </Card>
        <Card>
          <Card.Content >
            <TextInput
              label='Name'
              Type="outlined"
              value={petdetail.name}
              onChangeText={text => {
                let petdetail = { ...this.state.petdetail }
                petdetail.name = text;
                this.setState({ petdetail, valueChanged: true })
              }}
            />
            <TextInput
              label='Breed'
              style={{ marginTop: 10 }}
              value={petdetail.breed}
              onChangeText={text => {
                let petdetail = { ...this.state.petdetail }
                petdetail.breed = text;
                this.setState({ petdetail, valueChanged: true })
              }}
            />
            <TextInput
              label='Color'
              style={{ marginTop: 10 }}
              value={petdetail.colour}
              onChangeText={text => {
                let petdetail = { ...this.state.petdetail }
                petdetail.colour = text;
                this.setState({ petdetail, valueChanged: true })
              }}
            />
            <TextInput
              label='Age'
              keyboardType={'numeric'}
              style={{ marginTop: 10 }}
              value={petdetail.age}
              onChangeText={text => {
                let petdetail = { ...this.state.petdetail }
                petdetail.age = text;
                this.setState({ petdetail, valueChanged: true })
              }}
            />

            <Text style={{ marginTop: 20 }}>Next Checkup Date</Text>
            <DatePicker
              style={{ width: 200, marginTop: 10 }}
              date={petdetail.next_checkup} //initial date from state
              mode="date" //The enum of date, datetime and time
              placeholder="select date"
              format="YYYY-MM-DD"
              minDate={new Date()}
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  left: 0,
                  top: 4,
                  marginLeft: 0
                },
                dateInput: {
                  marginLeft: 36
                }
              }}
              onDateChange={(date) => {
                let petdetail = { ...this.state.petdetail }
                petdetail.next_checkup = date;
                this.setState({ petdetail, valueChanged: true })
              }}
            />
            <Text style={{ marginTop: 20 }}>Pet's vaccinations </Text>
            <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: 10 }}>
              <TagInput
                updateState={this.updateTagState}
                tags={this.state.tags}
                tagStyle={styles.tag}
                tagTextStyle={styles.tagText}
                inputContainerStyle={[styles.textInput]}
                placeholder="Add Vaccinations here.."
                keysForTag={', '}

              />
            </View>

          </Card.Content>
        </Card>
      </ScrollView>

    );
  }
}

const mapStateToProps = (state) => {
  return {
    pets: state.pets
  }

}

export default connect(mapStateToProps, { addPet, uploadImage })(AddPet)
