
'use strict';

const Bcrypt = require('bcrypt');
const Hapi = require('@hapi/hapi');
const CatboxRedis = require('@hapi/catbox-redis');


const users = {
    john: {
        username: 'john',
        password: '$2a$10$iqJSHD.BGr0E2IxQwYgJmeP3NvhPrXAeLSaGCj6IR/XU5QtjVu5Tm',   // 'secret'
        name: 'John Doe',
        id: '2133d32a'
    }
};

const validate = async (request, username, password) => {

    const user = users[username];
    if (!user) {
        return { credentials: null, isValid: false };
    }

    const isValid = await Bcrypt.compare(password, user.password);
    const credentials = { id: user.id, name: user.name };

    return { isValid, credentials };
};

const start = async () => {

    const server = Hapi.server({ port: 4000 ,
        cache: [
            {
                name: 'my_cache',
                provider: {
                    constructor: CatboxRedis,
                    options: {
                        partition : 'my_cached_data',
                        host: '127.0.0.1',
                        port: 6379,
                        database: 0
                    }
                }
            }
        ]});

    await server.register(require('@hapi/basic'));

    server.auth.strategy('simple', 'basic', { validate });
    const add = async (a, b) => {
        return Number(a) + Number(b);
    };

    const sumCache = server.cache({
        cache: 'my_cache',
        expiresIn: 10 * 1000,
        segment: 'customSegment',
        generateFunc: async (id) => {

            return await add(id.a, id.b);
        },
        generateTimeout: 2000
    });

    server.route({
        method: 'GET',
        path: '/add/{a}/{b}',
        options: {
            auth: 'simple'
        },
        handler: async function (request, h) {

            const { a, b } = request.params;
            const id = `${a}:${b}`;

            return await sumCache.get({ id, a, b });
        }
    });

    await server.start();

    console.log('server running at: ' + server.info.uri);
    return server
};

start();