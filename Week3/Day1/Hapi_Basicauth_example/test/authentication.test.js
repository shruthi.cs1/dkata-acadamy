const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { start } = require('../authentication');
const md5 = require('md5')
const btoa = require('btoa')
const Bcrypt = require('bcrypt');

const internals = {};


describe('GET /', () => {
    let server;

    beforeEach(async () => {
        server = await start();
    });

    afterEach(async () => {
        await server.stop();
    });

    it('returns a reply on successful auth', async () => {

        
        let username = "john";
        let hashPassword = "secret";
        var base64Encode = btoa(username + ':' + hashPassword);
        let authorization = 'Basic ' + base64Encode;
        const request = { method: 'GET', url: '/', headers: { authorization: authorization} };
        const res = await server.inject(request);
        expect(res.statusCode).to.equal(200);
    });

  
});