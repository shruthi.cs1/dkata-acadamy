const pets = require('../pets.json')

const sortByKey = (a, b,key) => {
    if (a[key] > b[key]) {
      return 1;
    }
    if (a[key] < b[key]) {
      return -1;
    }
    return 0;
  };

exports.getPetsHandler = function (request, h) {
    let sortValue = request.query.sort
    let sortedPets = pets.sort((a,b)=>sortByKey(a,b,sortValue))
    let limit = request.query.limit
    let offset = request.query.offset
    return h.response(sortedPets.slice(offset,parseInt(limit) + parseInt(offset))).code(200)
    
}

exports.getPetDetailsByIdHandler = function (request, h) {
    return pets.filter(eachPet => eachPet.id === request.params.petId)
    
}


exports.addPetHandler = function (request, h) {
    let payload = request.payload
    let maxValue = 0
    maxValue = pets.map(a => a.id).reduce(function(maxValue,b){
        return Math.max(maxValue,b)
    })
    payload["id"] = maxValue + 1
    pets.push(payload)
    return h.response(payload).code(201)
    
}

exports.updatePetHandler = function (request, h) {
    let payload = request.payload
    let toGetUpdate = pets.find(eachPet => eachPet.id == request.params.petId)
    toGetUpdate["next_checkup"] = payload.next_checkup
    return toGetUpdate
    
}

exports.markAsSoldPet = function (request,h){
    let toGetUpdate = pets.find(eachPet => eachPet.id == request.params.petId)
    toGetUpdate["sold"] = true
    return toGetUpdate

}

exports.notFoundHandler = function (request, h) {
    return h.response('The page was not found').code(404)
}