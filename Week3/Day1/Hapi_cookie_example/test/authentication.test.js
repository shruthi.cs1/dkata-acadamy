const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { start } = require('../authentication');
const Bcrypt = require('bcrypt');

const internals = {};


describe('GET /', () => {
    let server;

    beforeEach(async () => {
        server = await start();
    });

    afterEach(async () => {
        await server.stop();
    });

    it('returns a reply on successful auth', async () => {

        
       
        const request = { method: 'POST', url: '/login', payload: {username:"john",password:"secret"} };
        const res = await server.inject(request);
        const afterAuth = { method: 'GET', url: '/'}
        const afterres = await server.inject(afterAuth);
        console.log("afterres::",afterres)
        expect(res.statusCode).to.equal(302);
    });

  
});