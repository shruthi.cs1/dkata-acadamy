
const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { init } = require('../server');
const btoa = require('btoa')

const authorization = "Basic " + btoa("john:secret")

describe('GET /', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
    });

    it('Checking Get request status code : Not Found', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/',
            headers: 
            { authorization}
        });
        expect(res.statusCode).to.equal(404);
    });

    it('Getting pets data , and checking status code', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/v1/pets',
            headers: 
            { authorization}
        });
        expect(res.statusCode).to.equal(200);
    });

    it('Getting a pet details , where petId is 4 ', async () => {
        const res = await server.inject({
            method: 'get',
            url: '/v1/pets/4',
            headers: 
            { authorization}
        });

        let output = res.result.reduce((out,eachpet) => { return eachpet})
        expect(res.statusCode).to.equal(200);
        expect(output.id).to.equal(4)
        expect(res.result.length).to.equal(1)

    });

    it('Post a pet', async () => {
        let payload = {
            "name": "tomy465",
            "breed": "labrador retriever",
            "colour": "chocolate",
            "age": 5.4,
            "next_checkup": "2020-6-25",
            "vaccinations": [
                "Distemper shot",
                "Kennel Cough",
                "Dog Flu",
                "Rabies"
            ]
        }
        const res = await server.inject({
            method: 'POST',
            url: '/v1/pets',
            payload,
            headers: 
            { authorization}
        });

        expect(res.statusCode).to.equal(201);
       

    });

    it('Update a pet next_checkup', async () => {
        let payload = {
            "next_checkup": "2020-6-25"
        }
        const res = await server.inject({
            method: 'PUT',
            url: '/v1/pets/2',
            payload,
            headers: 
            { authorization}
        });

        expect(res.statusCode).to.equal(200);
       

    });

    it('Mark as sold: Soft Delete , without authenticarion :401', async () => {
       
        const res = await server.inject({
            method: 'DELETE',
            url: '/v1/pets/2'
        });

        expect(res.statusCode).to.equal(401);
       

    });

    it('Mark as sold: Soft Delete', async () => {
       
        const res = await server.inject({
            method: 'DELETE',
            url: '/v1/pets/2' ,
            headers: 
            { authorization}
        });

        expect(res.statusCode).to.equal(200);
       

    });

   
});