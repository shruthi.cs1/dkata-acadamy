const Hapi = require('@hapi/hapi')
const Joi = require('@hapi/joi');
const handler = require('./handlers/handlers')
const Bcrypt = require('bcrypt');

const users = {
    john: {
        username: 'john',
        password: '$2a$10$iqJSHD.BGr0E2IxQwYgJmeP3NvhPrXAeLSaGCj6IR/XU5QtjVu5Tm',   // 'secret'
        name: 'John Doe',
        id: '2133d32a'
    }
};

const validate = async (request, username, password) => {

    const user = users[username];
    if (!user) {
        return { credentials: null, isValid: false };
    }

    const isValid = await Bcrypt.compare(password, user.password);
    const credentials = { id: user.id, name: user.name };

    return { isValid, credentials };
};

const init = async () => {

    const server = Hapi.server({
        port: 3000,
        host: 'localhost'
    })

    await server.register(require('@hapi/basic'));

    await server.auth.strategy('simple', 'basic', { validate });

    server.route({
        method: 'GET',
        path: '/v1/pets',
        handler: handler.getPetsHandler,
        options: {
            validate: {
                query: {
                    sort: Joi.string().regex(/^[a-zA-Z]+$/),
                    limit: Joi.number().integer().default(2),
                    offset: Joi.number().integer().default(0)
                }
            },
            auth: 'simple'
        },
        
    })

    server.route({
        method: 'GET',
        path: '/v1/pets/{petId}',
        handler: handler.getPetDetailsByIdHandler,
        options: {
            validate: {
                params: {
                    petId: Joi.number()
                }
            },
            auth: 'simple'
        },
    })

    server.route({
        method: 'POST',
        path: '/v1/pets',
        handler: handler.addPetHandler,
        options:{
            auth: 'simple'
        }
    })

    server.route({
        method: 'PUT',
        path: '/v1/pets/{petId}',
        handler: handler.updatePetHandler,
        options: {
            validate: {
                params: {
                    petId: Joi.number()
                }
            },
            auth: 'simple'
        },
    })

    server.route({
        method: 'DELETE',
        path: '/v1/pets/{petId}',
        handler: handler.markAsSoldPet,
        options: {
            validate: {
                params: {
                    petId: Joi.number()
                }
            },
            auth: 'simple'
        },
    })

   

    server.route({
        method: '*',
        path: '/{p*}',
        handler: handler.notFoundHandler
    })

    await server.start()
    console.log('Server running on %s', server.info.uri)
    return server
}

process.on('unhandledRejection', (err) => {
    console.log(err)
    process.exit(1)
});

module.exports={init}

const runOrNot=process.argv[2]
if(runOrNot=='run'){
    init()
}


