const {PetModel} = require('../model/petModel')

const notFoundResponse={
    statusCode:404,
    error:'Not Found',
    message:'Not found'
}
const badRequestResponse={
    statusCode:400,
    error:'Bad Request',
    message:'Invalid request input'
}

const conflictResponse={
    statusCode:409,
    error:'Conflict',
    message:'Conflict request input'
}



exports.getPets = async (query,sort,offset,limit) =>{

    return await PetModel.find(query).sort({[sort]:1}).limit(limit).skip(offset).lean()
}

exports.getEachPet = async (petId) => {
    const result = await PetModel.find({id:petId}).lean()
    if(result){
        return result
    }else{
        return notFoundResponse
    }
     
}

exports.addPet = async (request) =>{
    const pets=await PetModel.aggregate([
        {
            $group:{
                _id:'max',
                maxId:{
                    $max:'$id',
                },
                names:{
                    $push:'$name'
                }
            }
        }
    ])  
    
    let nextId=pets.length > 0 ? pets[0].maxId+1 : 1
    Object.assign(request.payload,{id:nextId,sold:false})
    try{
        const result = await PetModel.insertMany([request.payload])
        return result
    }catch(error){
        if(error && error.err && error.err.code){
            switch(error.err.code){
                case 11000: return conflictResponse;
                default : return  badRequestResponse
            }

        }else{
            return badRequestResponse
        }
    }
   
    
    
}

exports.updatePet = async (request) => {
    let result = await PetModel.findOneAndUpdate({id:request.params.petId,sold:false},request.payload,{new:true,}).lean() 
    if(result){
        return  result 
    } else{
        return notFoundResponse
    }  
    
}


