const Mongoose = require('mongoose');
const db = Mongoose.connect('mongodb://localhost/petsDB',{useNewUrlParser:true,});
const Schema = Mongoose.Schema;
Mongoose.set('useCreateIndex', true);
Mongoose.set('debug', true);


//------ Checking the connection ---------- //
dbconnection = Mongoose.connection
dbconnection.on('error', console.error.bind(console, 'connection error'));
dbconnection.once('open', function callback() {
console.log('Connection with database succeeded!!! Yayyy!!');
});

module.exports = {db,Schema,Mongoose};