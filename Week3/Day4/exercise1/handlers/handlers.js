const fs = require('fs')
const pets = require('../pets.json')
const {getPets,addPet,getEachPet,updatePet} = require('../DAO/DBqueries')



exports.getPetsHandler = async function (request, h) {
    const queryParameter=request.query
    const isEmpty=JSON.stringify(queryParameter)=='{}'
    
    if(isEmpty){

        const result= await getPets({sold:false},'name',0,10)
        return h.response(result).code(200)
    } else {
        let sortBy=queryParameter.sort?queryParameter.sort:'name'
        let offset=queryParameter.offset?parseInt(queryParameter.offset):0
        let limit=queryParameter.limit?parseInt(queryParameter.limit):10
        let filter=queryParameter.filter?queryParameter.filter:{sold:false}
        const result=await getPets(filter,sortBy,offset,limit)
        return h.response(result).code(200)
    }
}

exports.getPetDetails = async (petId)=>{
    return await getEachPet(petId)
}


exports.getPetDetailsByIdHandler = async function(request, h) {
    const petId = request.params.petId
    const resultdetail=await request.server.methods.getPetDetails(petId)
    return h.response(resultdetail).code(200)

    
    
}


exports.addPetHandler = async function (request, h) {
    const result = await addPet(request)
    return  h.response(result).code(201)
    
}

exports.updatePetHandler = async function (request, h) {
    const result = await updatePet(request)
    await request.server.methods.getPetDetails.cache.drop(request.params.petId)
    return  h.response(result).code(202)
    
}

exports.markAsSoldPet = async function (request,h){
    
    request["payload"] = {}
    request["payload"]["sold"]= true
   const result = await updatePet(request)
    return  h.response(result).code(202)

}

exports.uploadFile = async function(request){
    const data = request.payload;
    const time = new Date()
    try{
        if (data.file) {
            const name = time.getTime() + data.file.hapi.filename;
            const path =  __dirname + "/uploads/" + name;
            const file = fs.createWriteStream(path);
    
            file.on('error', (err) => console.error(err));
    
            data.file.pipe(file);
    
            data.file.on('end', (err) => { 
                const ret = {
                    filename: data.file.hapi.filename,
                    headers: data.file.hapi.headers
                }
                
            })
    
            return name;
    
        }

    }catch(err){
        console.log('errorrrrrr:::',err)

    }
   
}


exports.notFoundHandler = function (request, h) {
    return h.response('The page was not found').code(404)
}