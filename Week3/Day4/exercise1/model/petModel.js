const {Schema,Mongoose} = require('../DAO/DBConnection')

const PetSchema = new Schema({
    id:Number,
    name:{type:String,unique:true,required:true},
    breed:String,
    colour:String,
    age:Number,
    date:Date,
    vaccinations:[String],
    sold:Boolean,
    next_checkup:{type:Date},
    image_url : String
});


const PetModel = Mongoose.model('pets',PetSchema)

module.exports = {PetModel}
