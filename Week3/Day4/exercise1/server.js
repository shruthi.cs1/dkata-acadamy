const Hapi = require('@hapi/hapi')
const Joi = require('@hapi/joi');
const handler = require('./handlers/handlers')
const CatboxRedis = require('@hapi/catbox-redis')
const Path = require('path')

const init = async () => {

    const server = Hapi.server({
        port: 3000,
        host: '0.0.0.0',
        routes: {
            files: {
                relativeTo: Path.join(__dirname, 'handlers/uploads')
            }
        },
        cache: [
            {
                name: 'pets_cache',
                provider: {
                    constructor: CatboxRedis,
                    options: {
                        partition : 'pets_cached_data',
                        host: 'localhost',
                        port: 6379
                    }
                }
            }
        ]
    })

    await server.register({
        plugin: require('hapi-pino'),
        options: {
          prettyPrint: process.env.NODE_ENV !== 'production',
          redact: ['req.headers.authorization'],
          stream:'logs.log'
        }
      })

      await server.register(require('inert'));
    
    server.method('getPetDetails',handler.getPetDetails,{
        cache:{
            cache:'pets_cache',
            segment:'pets',
            expiresIn:1000*1000,
            generateTimeout:2000
        }
    })

    server.route({
        method: 'GET',
        path: '/v1/profile/{name}',
        handler: function (request, h) {

            return h.file(request.params.name);
        }
    });


    server.route({
        method: 'GET',
        path: '/v1/pets',
        handler: handler.getPetsHandler,
        options: {
            validate: {
                query: {
                    sort: Joi.string().regex(/^[a-zA-Z]+$/),
                    limit: Joi.number().integer().default(2),
                    offset: Joi.number().integer().default(0)
                }
            }
        },
        
    })

    server.route({
        method: 'GET',
        path: '/v1/pets/{petId}',
        handler: handler.getPetDetailsByIdHandler,
        options: {
            validate: {
                params: {
                    petId: Joi.number()
                }
            }
        },
    })

    server.route({
        method: 'POST',
        path: '/v1/pets',
        handler: handler.addPetHandler,
        options:{
            validate:{
                payload:{
                    id:Joi.forbidden(),
                    name:Joi.string().regex(/^[a-zA-Z0-9\s]+$/).required(),
                    breed:Joi.string().regex(/^[a-zA-Z\s]+$/).required(),
                    colour:Joi.string().regex(/^[a-zA-Z\s]+$/).required(),
                    age:Joi.number().min(0).required(),
                    next_checkup:Joi.date().required(),
                    vaccinations:Joi.array().required(),
                    sold:Joi.forbidden(),
                    image_url:Joi.string()
                }
            }
        }

    })

    server.route({
        method: 'POST',
        path: '/v1/upload',
        handler: handler.uploadFile,
        options:{
            payload:{
                output: 'stream',
                parse: true,
                allow: 'multipart/form-data',
            }
        }

    })

    server.route({
        method: 'PUT',
        path: '/v1/pets/{petId}',
        handler: handler.updatePetHandler,
        options: {
            validate: {
                params: {
                    petId: Joi.number()
                },
                payload:{
                    age:Joi.number().min(0).required(),
                    next_checkup:Joi.date().required(),
                    vaccinations:Joi.array().required(),
                    image_url:Joi.string()
                }
                
            }
        },
    })

    server.route({
        method: 'DELETE',
        path: '/v1/pets/{petId}',
        handler: handler.markAsSoldPet,
        options: {
            validate: {
                params: {
                    petId: Joi.number()
                }
            }
        },
    })

   

    server.route({
        method: '*',
        path: '/{p*}',
        handler: handler.notFoundHandler
    })

    await server.start()
    console.log('Server running on %s', server.info.uri)
    return server
}

process.on('unhandledRejection', (err) => {
    console.log(err)
    process.exit(1)
});

module.exports={init}

const runOrNot=process.argv[2]
if(runOrNot=='run'){
    init()
}


