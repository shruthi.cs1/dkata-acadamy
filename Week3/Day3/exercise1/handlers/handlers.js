const pets = require('../pets.json')
const {getPets,addPet,getEachPet,updatePet} = require('../DAO/DBqueries')


exports.getPetsHandler = async function (request, h) {
    const queryParameter=request.query
    const isEmpty=JSON.stringify(queryParameter)=='{}'
    
    if(isEmpty){

        const result= await getPets({sold:false},'name',0,10)
        return h.response(result).code(200)
    } else {
        let sortBy=queryParameter.sort?queryParameter.sort:'name'
        let offset=queryParameter.offset?parseInt(queryParameter.offset):0
        let limit=queryParameter.limit?parseInt(queryParameter.limit):10
        let filter=queryParameter.filter?queryParameter.filter:{sold:false}
        const result=await getPets(filter,sortBy,offset,limit)
        return h.response(result).code(200)
    }
}


exports.getPetDetailsByIdHandler = async function(request, h) {
    const result = await getEachPet(request)
    return  h.response(result).code(200)
    
}


exports.addPetHandler = async function (request, h) {
    const result = await addPet(request)
    return  h.response(result).code(201)
    
}

exports.updatePetHandler = async function (request, h) {
    const result = await updatePet(request)
    return  h.response(result).code(202)
    
}

exports.markAsSoldPet = async function (request,h){
    
    request["payload"] = {}
    request["payload"]["sold"]= true
   const result = await updatePet(request)
    return  h.response(result).code(202)

}

exports.notFoundHandler = function (request, h) {
    return h.response('The page was not found').code(404)
}