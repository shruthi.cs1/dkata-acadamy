const Hapi = require('@hapi/hapi')
const Joi = require('@hapi/joi');
const handler = require('./handlers/handlers')

const init = async () => {

    const server = Hapi.server({
        port: 3000,
        host: 'localhost'
    })

    server.route({
        method: 'GET',
        path: '/v1/pets',
        handler: handler.getPetsHandler,
        options: {
            validate: {
                query: {
                    sort: Joi.string().regex(/^[a-zA-Z]+$/),
                    limit: Joi.number().integer().default(2),
                    offset: Joi.number().integer().default(0)
                }
            }
        },
        
    })

    server.route({
        method: 'GET',
        path: '/v1/pets/{petId}',
        handler: handler.getPetDetailsByIdHandler,
        options: {
            validate: {
                params: {
                    petId: Joi.number()
                }
            }
        },
    })

    server.route({
        method: 'POST',
        path: '/v1/pets',
        handler: handler.addPetHandler,
        options:{
            validate:{
                payload:{
                    id:Joi.forbidden(),
                    name:Joi.string().regex(/^[a-zA-Z0-9\s]+$/).required(),
                    breed:Joi.string().regex(/^[a-zA-Z\s]+$/).required(),
                    colour:Joi.string().regex(/^[a-zA-Z\s]+$/).required(),
                    age:Joi.number().min(0).required(),
                    next_checkup:Joi.date().required(),
                    vaccinations:Joi.array().required(),
                    sold:Joi.forbidden()
                }
            }
        }

    })

    server.route({
        method: 'PUT',
        path: '/v1/pets/{petId}',
        handler: handler.updatePetHandler,
        options: {
            validate: {
                params: {
                    petId: Joi.number()
                }
            }
        },
    })

    server.route({
        method: 'DELETE',
        path: '/v1/pets/{petId}',
        handler: handler.markAsSoldPet,
        options: {
            validate: {
                params: {
                    petId: Joi.number()
                }
            }
        },
    })

   

    server.route({
        method: '*',
        path: '/{p*}',
        handler: handler.notFoundHandler
    })

    await server.start()
    console.log('Server running on %s', server.info.uri)
    return server
}

process.on('unhandledRejection', (err) => {
    console.log(err)
    process.exit(1)
});

module.exports={init}

const runOrNot=process.argv[2]
if(runOrNot=='run'){
    init()
}


