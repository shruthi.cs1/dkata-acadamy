const Mongoose = require('mongoose');
const db = Mongoose.connect('mongodb://localhost/petsDB',{useNewUrlParser:true,useFindAndModify:true});
const Schema = Mongoose.Schema;

//------ Checking the connection ---------- //
dbconnection = Mongoose.connection
dbconnection.on('error', console.error.bind(console, 'connection error'));
dbconnection.once('open', function callback() {
console.log('Connection with database succeeded!!! Yayyy!!');
});

module.exports = {db,Schema,Mongoose};