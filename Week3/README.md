Day 1:

Authentication and Authorization in hapi
Unit Testing of Basic Authentication in hapi
About node package btoa
Respective HTTP Status Codes 
DELETE REST API.


Day 2:

What is MongoDB?
Why and when to use MongoDB.
Creating and Dropping Database 
Creating, updating, inserting and deleting Document 
Methods in performing a query 
Projection 
Limiting 
Sorting 
Mapreduce
Aggregation


Day 3:

Indexes
Data Modeling
REST Apis using mongoose in hapi 
Unit Test for respective 
What is the best way to send Filters in URL params
About npm “qs”, How to parse Query String  


Day 4:

Logging using hapi-Pino
Writing logs into the files.
Writing server methods to cache using catbox in hapi 
To remove cache on update using drop-in hapi  
