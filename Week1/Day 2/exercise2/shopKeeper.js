const counter = function () {
    let currentValue = 0
    const increment = function () {
        currentValue += 1
    }
    const decrement = function () {
        currentValue -= 1
    }
    const getCurrentValue = function () {
        return currentValue
    }

    return { increment, decrement, getCurrentValue }
}

let myCounter = counter()
myCounter.increment()
myCounter.increment()
myCounter.decrement()


let anotherCounter = counter()

module.exports = { counter }
