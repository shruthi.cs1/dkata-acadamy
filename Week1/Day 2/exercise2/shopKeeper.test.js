const shopKeeper = require('./shopKeeper')
const testCounter = shopKeeper.counter()

test('expect counter to start at 0', () => {
    expect(testCounter.getCurrentValue()).toEqual(0)
})

test('expect counter to be at 2', () => {
    testCounter.increment()
    testCounter.increment()
    expect(testCounter.getCurrentValue()).toBe(2)
})

test('expect counter to be at 1', () => {
    testCounter.decrement()
    expect(testCounter.getCurrentValue()).toBe(1)
})
