var calculate = require('./exponents')


test('Get square of a number',() =>{
    expect(calculate.square(2)).toBe(4)
})

test('Get cube of a number',() =>{
    expect(calculate.cube(4)).toBe(64)
})

test('Get exponents of two numbers',() =>{
    expect(calculate.exponent(1000,1)).toBe(1000)
})

test('Get exponents of two numbers',() =>{
    expect(calculate.exponent(1000,2)).toBe(1000000)
})

test('Get square of two numbers',() =>{
    expect(calculate.square(2)).toBe(4)
})

test('Get cube of two numbers',() =>{
    expect(calculate.cube(2)).toBe(8)
})

