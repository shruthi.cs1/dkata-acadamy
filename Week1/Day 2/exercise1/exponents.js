const exponent = function(a,b){
    return a ** b
}

const square = function(num){
    return exponent(num,2)
}

const cube = function(num){
    return exponent(num,3)
}

module.exports = { square , cube, exponent}