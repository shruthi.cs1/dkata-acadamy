const { trainee, trainer } = require('./person')

let Shruthi = new trainee('Shruthi','cs','27','female','Bangalore, India','Dancing')
let radha = new trainer('Radha','Reddy','27','male','Bangalore, India', 'Talking')

test('The trainee instance to show name,age,address and interest', () => {
  expect(Shruthi.greeting()).toBe('Hi all!! I\'m Shruthi cs, my age is 27, i came from Bangalore, India and i\'ve insterest to Dancing!!')
})

test('The trainer instance to show address', () => {
  expect(radha.address).toBe('Bangalore, India')
})

