class person {
    constructor(first, last, age, gender, address, type) {
        this.first = first
        this.last = last
        this.age = age
        this.gender = gender
        this.address = address
        this.type = type
        this.greeting = function(){
            switch (this.type) {
                case ('trainer'):
                    return `Hi all!! I\'m ${this.first} ${this.last}, my age is ${this.age}, i came from ${this.address} and my skill is ${this.skill}!!`;
                case ('trainee'):
                    return `Hi all!! I\'m ${this.first} ${this.last}, my age is ${this.age}, i came from ${this.address} and i\'ve insterest to ${this.interest}!!`;
                default:
                    return `Hi all!! I\'m ${this.first} ${this.last}, my age is ${this.age}, i came from ${this.address}`
            }
        }
        
    }
}

class trainer extends person {
    constructor(first, last, age, gender, address, skill) {
        super(first, last, age, gender, address, 'trainer')
        this.skill = skill
    }
}
class trainee extends person {
    constructor(first, last, age, gender, address, interest) {
        super(first, last, age, gender, address, 'trainee')
        this.interest = interest
    }
}

module.exports = {
    person,
    trainer,
    trainee
}
