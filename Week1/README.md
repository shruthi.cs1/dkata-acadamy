Dkata Academy

Daily Exercises solution will be available in this repo 

Day 1 :

Basic Unix Commands  
Create/Remove/Copy a file or a directory 
List Directory    
Change Permission 
Process Status
Search in File 
Piping          

Git 
What is source control?
Why is it important?
How does it work? 

Day 2 :

Basic of Javascript 
Syntax, Javascript Primitive and non-primitive data type
Javascript keywords
JavaScript operators
Javascript conditions
Javascript loops

Functional programing 
Function composition, 
Currying, 
Functions as args, 
Functions returning functions(closures) 

Unit Testing 
What is testing?
Why unit testing is important ?
Test from best to worst cases


Day 3 :

OOP Concepts :
Constructor
Encapsulation
Abstraction
Inheritance
Polymorphism

OOJS - Object-Oriented JavaScript: Everything in javascript is an object. 
How to create an object?
How to access an object?
Add and remove key value to an object.

Advanced JS:
Prototypes 


