const fruits = ['apple', 'durian', 'banana']
const expected = [5, 6, 6]
const expectedgt5 = 2

const lengths = fruits.map(el => el.length)
const lengthsgt5 = fruits.map(el => el.length).filter(el => el > 5).length

console.log(lengths)
console.log(lengthsgt5)

console.assert(lengths.length === expected.length && lengths.every((value, index) => value === expected[index]), 'oops')
console.assert(lengthsgt5 === expectedgt5, 'oops')