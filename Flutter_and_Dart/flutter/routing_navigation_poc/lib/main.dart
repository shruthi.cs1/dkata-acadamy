import 'package:flutter/material.dart';
import "./src/Widgets/Home.dart";
import './src/Widgets/SecondHome.dart';
import "./src/Widgets/SlideRight.dart";

void main() {
  runApp(MaterialApp(
    home: HomePage(),
    onGenerateRoute: (RouteSettings settings) {

      switch (settings.name) {
        case '/':
          return SlideRightRoute(widget:HomePage());
          break;
        case '/second':
          return SlideRightRoute(widget:SecondHome());
          break;
      }
    },
  ));
}

