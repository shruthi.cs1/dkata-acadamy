import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import "./app_config.dart";


void main({String env}) async {
  // load our config
  WidgetsFlutterBinding.ensureInitialized();

  final config = await AppConfig.forEnvironment(env);

  // pass our config to our app
  runApp(MyApp(config:config));
}
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final config;
  MyApp({Key key, this.config}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page',config:config),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title,this.config}) : super(key: key);
  final String title;
  final config;
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  Map randomJson = {"title":"Nothing"};

  void  _incrementCounter() async {

    _counter = _counter + 1;


    // Calling a api to check
    final response = await http.get('${widget.config.apiUrl}/posts/$_counter');

    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      randomJson = jsonDecode(response.body);
      setState(() {
        _counter;
        randomJson;
      });
    } else {
      // If that response was not OK, throw an error.
      throw Exception('Failed to load get');
    }


  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Press button to get different title',
            ),
            Text(
              'Title of id $_counter is  ${randomJson["title"]}',
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), 
    );
  }
}
