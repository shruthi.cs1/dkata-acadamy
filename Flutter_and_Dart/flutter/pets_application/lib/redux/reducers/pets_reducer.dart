import 'package:pets_application/model/app_state.dart';
import 'package:pets_application/redux/actions/pets_action.dart';
import 'package:redux/redux.dart';

AppState appStateReducer(AppState state, action) {
  return PetsReducer(state, action);
}

Reducer<AppState> PetsReducer = combineReducers<AppState>([
  TypedReducer<AppState, SetApiUrl>(setApiUrlReducer),
  TypedReducer<AppState, GettingPetSuccess>(gettingPetSuccessReducer),
  TypedReducer<AppState, GettingPetFailed>(gettingPetFailedReducer),
  TypedReducer<AppState, AddedPetSuccess>(addedPetSuccessReducer),
  TypedReducer<AppState, AddedPetFailed>(addedPetFailedReducer),
  TypedReducer<AppState, UpdatePetSuccess>(updatePetSuccessReducer),
  TypedReducer<AppState, UpdatePetFailed>(updatePetFailedReducer),
  TypedReducer<AppState, UploadImageSuccess>(uploadImageSuccessReducer),
  TypedReducer<AppState, UploadImageFailure>(uploadImageFailureReducer)
]);

AppState setApiUrlReducer(AppState state, SetApiUrl action) {
  return state.copyWith(apiUrl: action.apiUrl);
}

AppState gettingPetSuccessReducer(AppState state, GettingPetSuccess action) {
  return state.copyWith(isLoading: false, petList: action.pets);
}

AppState gettingPetFailedReducer(AppState state, GettingPetFailed action) {
  return state.copyWith(isLoading: false, error: action.error);
}

AppState addedPetSuccessReducer(AppState state, AddedPetSuccess action) {
  final petsList = state.petList;
  petsList.add(action.pet);
  return state.copyWith(isLoading: false, petList: petsList);
}

AppState addedPetFailedReducer(AppState state, AddedPetFailed action) {
  return state.copyWith(error: action.error);
}

AppState updatePetSuccessReducer(AppState state, UpdatePetSuccess action) {
  final petList = state.petList;
  final index =
      petList.indexWhere((eachpet) => eachpet['id'] == action.petDetails['id']);
  petList[index] = action.petDetails;
  return state.copyWith(isLoading: false, petList: petList);
}

AppState updatePetFailedReducer(AppState state, UpdatePetFailed action) {
  return state.copyWith(error: action.error);
}

AppState uploadImageSuccessReducer(AppState state, UploadImageSuccess action) {
  return state.copyWith(imagePath: action.imagePath);
}

AppState uploadImageFailureReducer(AppState state, UploadImageFailure action) {
  return state.copyWith(error: action.error);
}
