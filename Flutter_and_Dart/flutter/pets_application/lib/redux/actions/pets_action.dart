import 'package:dio/dio.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';
import 'dart:convert';
import "../../model/app_state.dart";
import 'package:flutter/services.dart' show rootBundle;
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';


class SetApiUrl {
  final String apiUrl;
  SetApiUrl(this.apiUrl);
}

class GettingPetSuccess {
  final List  pets;
  GettingPetSuccess(this.pets);
}

class GettingPetFailed {
  final String error;
  GettingPetFailed(this.error);
}

class AddedPetSuccess{
  final Map pet;
  AddedPetSuccess(this.pet);

}

class AddedPetFailed{
  final String error;
  AddedPetFailed(this.error);
}

class UpdatePetSuccess{
  final Map petDetails;
  UpdatePetSuccess(this.petDetails);
}

class UpdatePetFailed{
  final String error;
  UpdatePetFailed(this.error);
}

class UploadImageSuccess{
  final String imagePath;
  UploadImageSuccess(this.imagePath);
}

class UploadImageFailure{
  final String error;
  UploadImageFailure(this.error);
}

Future<String> loadBaseURL() async{
   
     String raw = await rootBundle.loadString('assets/config.json');
     Map result = json.decode(raw);
     return result["apiURL"];
   
}


ThunkAction<AppState> getPets() {
  return (Store<AppState> store) async {
    String uri = await loadBaseURL();
    store.dispatch(SetApiUrl(uri));
    final response = await http.get('$uri/pets?sort=id&limit=20&offset=0');
    if (response.statusCode == 200) {
      // If server returns an OK response, parse the JSON.
      final responseJson = jsonDecode(response.body);
      store.dispatch(GettingPetSuccess(responseJson));

     
    } else {
      // If that response was not OK, throw an error.
      store.dispatch(GettingPetFailed("error while getting"));
    }
   
  };
}

ThunkAction<AppState> addPet(petPayload) {
  return (Store<AppState> store) async {
    String uri = await loadBaseURL();
    Dio dio = Dio();
    await dio.post('$uri/pets',data:petPayload)
      .then((res){
          if(res.data.length > 0 && res.data[0].containsKey('id')){
                Map response = res.data[0];
                store.dispatch(AddedPetSuccess(response));
                Fluttertoast.showToast(
                  msg: "Added Pet Successfully",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIos: 1,
                  fontSize: 16.0
              );
          }else{
             Fluttertoast.showToast(
                  msg: res.data ? res.data["message"] : "Error while adding pet",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIos: 1,
                  fontSize: 16.0
              );
          }
        
      })
      .catchError((onError) {
        store.dispatch(AddedPetFailed("Error while adding Pet"));
      });
  };
}

ThunkAction<AppState> updatePet(petPayload,id) {
  return (Store<AppState> store) async {
    String uri = await loadBaseURL();
    Dio dio = Dio();
    await dio.put('$uri/pets/$id',data:petPayload)

      .then((res){
        store.dispatch(UpdatePetSuccess(res.data));
        
        })
      .catchError((onError) {
      
        store.dispatch(UpdatePetFailed("Error while Updating pet."));
      });
  };
}

ThunkAction<AppState> uploadImage(file,petpayload,type,id) {
  return (Store<AppState> store) async {
    String uri = await loadBaseURL();
    Dio dio = Dio();
    FormData formData = FormData.fromMap(Map<String, dynamic>.from(file));
    await dio.post('$uri/upload',data:formData)
      .then((res) {
          petpayload["image_url"] = res.data;
          if(type == 'add'){
           
            store.dispatch(addPet(petpayload));
          }else{
            store.dispatch(updatePet(petpayload,id));
          }
          store.dispatch(UploadImageSuccess(res.data));
      })
      .catchError((onError) {
        store.dispatch(UploadImageFailure("Error while uploading image"));
      });
  };
}



