import "package:flutter/material.dart";
import "package:pets_application/model/app_state.dart";
import "package:pets_application/redux/actions/pets_action.dart";
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import "package:pets_application/widget/editPetWidget.dart";
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';

class PetListWidget extends StatefulWidget {
  @override
  _PetListWidgetState createState() => _PetListWidgetState();
}

class _PetListWidgetState extends State<PetListWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: StoreConnector<AppState, _ViewModel>(
            onInit: (store) {
              store.dispatch(getPets());
            },
            converter: (Store<AppState> store) => _ViewModel.create(store),
            builder: (BuildContext context, _ViewModel viewModel) =>
                _bodyPage(viewModel)));
  }

  Future<String> loadBaseURL() async {
    String raw = await rootBundle.loadString('assets/config.json');
    Map result = json.decode(raw);
    return result['apiURL'];
  }

  _bodyPage(viewModel) {
    final _ViewModel model = viewModel;
    final apiUrl = model.appState.apiUrl;

    return Scaffold(
        appBar: AppBar(
          title: Text('Pets'),
        ),
        body: _myListView(context, model, apiUrl),
        floatingActionButton: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            FloatingActionButton(
              onPressed: () {
                Navigator.pushNamed(context, '/addpet');
              },
              tooltip: 'Add Pet',
              child: Icon(Icons.add),
            )
          ],
        ));
  }

  Widget _myListView(BuildContext context, model, apiUrl) {
    // backing data
    //Calling get pet api
    final petList = model.appState.petList;

    return ListView.builder(
      itemCount: petList.length,
      itemBuilder: (context, index) {
        return Card(
            margin: EdgeInsets.all(5.0),
            child: InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>
                          EditPetWidget(petDetail: petList[index]),
                    ),
                  );
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text(
                          'Name : ${petList[index]["name"]}',
                          style: TextStyle(fontSize: 15.0),
                        ),
                        Text(
                          'Breed : ${petList[index]["breed"]}',
                          style: TextStyle(fontSize: 15.0),
                        ),
                      ],
                    ),
                    Card(
                      elevation: 18.0,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.0))),
                      child: Image.network(
                        '$apiUrl/profile/${petList[index]["image_url"]}',
                        fit: BoxFit.cover,
                        height: 200.0,
                        width: 330.0,
                      ),
                      clipBehavior: Clip.antiAlias,
                      margin: EdgeInsets.all(9.0),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text(
                          'Age : ${petList[index]["age"].toString()}',
                          style: TextStyle(fontSize: 15.0),
                        ),
                        Text(
                          'Colour : ${petList[index]["colour"]}',
                          style: TextStyle(fontSize: 15.0),
                        )
                      ],
                    ),
                  ],
                )));
      },
    );
  }
}

class _ViewModel {
  final AppState appState;
  final Function() gettingPets;

  _ViewModel({this.appState, this.gettingPets});

  factory _ViewModel.create(Store<AppState> store) {
    _gettingPets() {
      store.dispatch(getPets());
    }

    return _ViewModel(appState: store.state, gettingPets: _gettingPets);
  }
}
