import 'package:flutter/material.dart';
import "package:pets_application/model/app_state.dart";
import "package:pets_application/redux/actions/pets_action.dart";
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import "package:pets_application/widget/sharedFunctions/validators.dart";
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter_tags/tag.dart';

class EditPetWidget extends StatefulWidget {
  final Map petDetail;
  EditPetWidget({Key key, this.petDetail}) : super(key: key);

  @override
  _EditPetWidgetState createState() => _EditPetWidgetState(this.petDetail);
}

class _EditPetWidgetState extends State<EditPetWidget> {
  final Map petDetails;
  static String name;
  static String breed;
  static String colour;
  static String age;
  List vaccinations = [];
  final _namecontroller = new TextEditingController();
  final _agecontroller = new TextEditingController();
  final _breedcontroller = new TextEditingController();
  final _colourcontroller = new TextEditingController();
  DateTime nextCheckUpDate = DateTime.now();
  _EditPetWidgetState(this.petDetails) {
    _namecontroller.text = this.petDetails['name'];
    vaccinations = this.petDetails["vaccinations"];
    _breedcontroller.text = this.petDetails["breed"];
    _colourcontroller.text = this.petDetails["colour"];
    _agecontroller.text = this.petDetails['age'].toString();
    nextCheckUpDate = DateTime.parse(this.petDetails['next_checkup']);
  }
  File _image;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StoreConnector<AppState, _ViewModel>(
          converter: (Store<AppState> store) => _ViewModel.create(store),
          builder: (BuildContext context, _ViewModel viewModel) =>
              _bodyPage(viewModel)),
      floatingActionButton: FloatingActionButton(
        onPressed: getImage,
        tooltip: 'Pick Image',
        child: Icon(Icons.add_a_photo),
      ),
    );
  }

  _bodyPage(viewModel) {
    final _ViewModel model = viewModel;

    return Scaffold(
      appBar: AppBar(
        title: Text('View/Update Pet'),
      ),
      body: _petView(context, model),
    );
  }

  Future chooseImage(source) async {
    var image = await ImagePicker.pickImage(source: source);
    setState(() {
      _image = image;
    });
  }

  Future getImage() async {
    return await showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return SimpleDialog(title: Text('Take a Photo'), children: <Widget>[
            SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context);
                  chooseImage(ImageSource.camera);
                },
                child: Text('Click a photo')),
            SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context);
                  chooseImage(ImageSource.gallery);
                },
                child: Text('Select from gallery')),
            SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text('Cancel'))
          ]);
        });
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: nextCheckUpDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != nextCheckUpDate)
      setState(() {
        nextCheckUpDate = picked;
      });
  }

  final GlobalKey<TagsState> _tagStateKey = GlobalKey<TagsState>();
// Allows you to get a list of all the ItemTags


  Widget _tagWidget(BuildContext context) {
    return Tags(
      key: _tagStateKey,
      textField: TagsTextField(
        textStyle: TextStyle(fontSize: 14),
        onSubmitted: (String str) {
          // Add item to the data source.
          setState(() {
            // required
            vaccinations.add(str);
          });
        },
      ),
      itemCount: vaccinations.length, // required
      itemBuilder: (int index) {
        final item = vaccinations[index];

        return ItemTags(
          // Each ItemTags must contain a Key. Keys allow Flutter to
          // uniquely identify widgets.
          key: Key(index.toString()),
          index: index, // required
          title: item,
          textStyle: TextStyle(
            fontSize: 14,
          ),
          combine: ItemTagsCombine.withTextBefore,
          removeButton: ItemTagsRemoveButton(),
          onRemoved: () {
            // Remove the item from the data source.
            setState(() {
              // required
              vaccinations.removeAt(index);
            });
          },
          onPressed: (item) => print(item),
          onLongPressed: (item) => print(item),
        );
      },
    );
  }

  Widget _petView(BuildContext context, model) {
    // backing data
    final validators = new Validators();
    final apiUrl = model.appState.apiUrl;

    final _formKey = GlobalKey<FormState>();
    return ListView(
        shrinkWrap: true,
        padding: const EdgeInsets.all(20.0),
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Center(
                      child: _image == null
                          ? Image.network(
                              "$apiUrl/profile/${this.petDetails['image_url']}")
                          : Image.file(_image,
                              fit: BoxFit.cover, height: 200.0, width: 330.0),
                    ),
                    Container(
                      child: Column(
                        children: <Widget>[
                          TextFormField(
                            readOnly: true,
                            controller: _namecontroller,
                            decoration: const InputDecoration(
                              hintText: 'Name of Pet?',
                              labelText: 'Name *',
                            ),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Please enter some text';
                              }
                              return validators.alphaNumericValidator(value);
                            },
                          ),
                          TextFormField(
                              readOnly: true,
                              controller: _breedcontroller,
                              decoration: const InputDecoration(
                                hintText: 'Pet Breed',
                                labelText: 'Breed *',
                              ),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Please enter some text';
                                }
                                return validators.alphaNumericValidator(value);
                              }),
                          TextFormField(
                              controller: _agecontroller,
                              decoration: const InputDecoration(
                                hintText: 'Pet Age?',
                                labelText: 'Age *',
                              ),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Please enter some text';
                                }
                                return validators.onlydouble(value);
                              }),
                          TextFormField(
                              readOnly: true,
                              controller: _colourcontroller,
                              decoration: const InputDecoration(
                                hintText: 'Pet Colour?',
                                labelText: 'Colour *',
                              ),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Please enter some text';
                                }
                                return validators.onlyCharacter(value);
                              }),
                          Row(
                            children: <Widget>[
                              SizedBox(
                                height: 20.0,
                              ),
                              RaisedButton(
                                onPressed: () => _selectDate(context),
                                child: Text('Click To Next CheckUp Date'),
                              ),
                              Text("${nextCheckUpDate.toLocal()}".split(' ')[0])
                            ],
                          ),
                          _tagWidget(context),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 16.0),
                            child: RaisedButton(
                              textColor: Colors.white,
                              color: Colors.red,
                              padding: const EdgeInsets.all(0.0),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(80.0)),
                              child: Container(
                                decoration: const BoxDecoration(
                                  gradient: LinearGradient(
                                    colors: [Colors.red, Colors.lightBlue],
                                  ),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(80.0)),
                                ),
                                padding:
                                    const EdgeInsets.fromLTRB(20, 10, 20, 10),
                                child: const Text('Uodate Pet',
                                    style: TextStyle(fontSize: 20)),
                              ),
                              onPressed: () async {
                                // Validate returns true if the form is valid, or false
                                // otherwise.
                                if (_formKey.currentState.validate()) {
                                  // If the form is valid, display a Snackbar.
                                  if (_image != null) {
                                    int dateNow =
                                        DateTime.now().millisecondsSinceEpoch;
                                    List fileDetail =
                                        _image.path.split('/').last.split('.');
                                    String fileName =
                                        '${fileDetail.first}$dateNow.${fileDetail.last}';
                                    model.uploadingimage({
                                      'name': fileName,
                                      'file': await MultipartFile.fromFile(
                                          _image.path,
                                          filename: fileName)
                                    }, {
                                      'age': int.parse(_agecontroller.text),
                                      'next_checkup':
                                          this.nextCheckUpDate.toString(),
                                      'vaccinations': this.vaccinations,
                                      'image_url': ""
                                    }, 'edit', this.petDetails['id']);
                                  } else {
                                    model.editingPet({
                                      'age': int.parse(_agecontroller.text),
                                      'next_checkup':
                                          this.nextCheckUpDate.toString(),
                                      'vaccinations': this.vaccinations,
                                      'image_url': this.petDetails['image_url']
                                    }, this.petDetails['id']);
                                  }
                                  Navigator.pop(context);
                                }
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          )
        ]);
  }
}

class _ViewModel {
  final AppState appState;
  final Function(Map, int) editingPet;
  final Function(Map, Map, String, int) uploadingimage;

  _ViewModel({this.appState, this.editingPet, this.uploadingimage});

  factory _ViewModel.create(Store<AppState> store) {
    _editPetDetails(Map payload, id) {
      store.dispatch(updatePet(payload, id));
    }

    _uploadImage(Map file, Map payload, String type, int id) {
      store.dispatch(uploadImage(file, payload, type, id));
    }

    return _ViewModel(
        appState: store.state,
        editingPet: _editPetDetails,
        uploadingimage: _uploadImage);
  }
}
