import 'package:flutter/material.dart';
import 'package:pets_application/widget/addPetWidget.dart';
import 'package:pets_application/widget/editPetWidget.dart';
import 'package:pets_application/widget/petListWidget.dart';

import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

import 'package:pets_application/model/app_state.dart';

class App extends StatelessWidget {
  final Store<AppState> store;

  const App({Key key, this.store}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
        store: store,
        child: MaterialApp(
            title: 'Photo Sharing App',
            initialRoute: '/petlist',
            theme: ThemeData.dark(),
            onGenerateRoute: _getRoute));
  }
}

Route<dynamic> _getRoute(RouteSettings settings) {
  switch (settings.name) {
    case '/petlist':
      return MaterialPageRoute(builder: (_) => PetListWidget());
    case '/addpet':
      return MaterialPageRoute(builder: (_) => AddPetWidget());
    case '/editpet':
      return MaterialPageRoute(builder: (_) => EditPetWidget());
    default:
      return null;
  }
}
