class AppState {
  final List petList;
  final String error;
  final String imagePath;
  final bool isLoading;
  final String apiUrl;

  AppState(
      {this.petList, this.error, this.imagePath, this.isLoading, this.apiUrl});

  AppState.initialState()
      : petList = List.unmodifiable([]),
        isLoading = false,
        imagePath = "",
        error = "",
        apiUrl = "";

  AppState copyWith(
      {List petList,
      String error,
      String imagePath,
      bool isLoading,
      String apiUrl}) {
    return AppState(
      petList: petList ?? this.petList,
      error: error ?? this.error,
      imagePath: imagePath ?? this.imagePath,
      isLoading: isLoading ?? this.isLoading,
      apiUrl: apiUrl ?? this.apiUrl,
    );
  }

  static AppState fromJSON(dynamic json) {
    if (json == null) {
      return AppState.initialState();
    }
    return AppState(
        petList: json['petList'] as List,
        isLoading: json['isLoading'] as bool,
        imagePath: json['loading'] as String,
        error: json['error'] as String,
        apiUrl: json['apiUrl'] as String);
  }

  dynamic toJson() => {
        'petList': [],
        'isLoading': false,
        'imagePath': "",
        'error': "",
        'apiUrl': ""
      };

  @override
  String toString() {
    return toJson().toString();
  }
}
