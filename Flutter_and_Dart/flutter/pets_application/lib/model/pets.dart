class Pet {
  final int id;
  final String name;
  final String breed;
  final String colour;
  final double age;
  final String next_checkup;
  final List vaccinations;
  final bool sold;
  final String image_url;

  Pet(
      {this.id,
      this.name,
      this.breed,
      this.colour,
      this.age,
      this.next_checkup,
      this.vaccinations,
      this.image_url,
      this.sold});

  Pet copyWith(
      {int id,
      String name,
      String breed,
      String colour,
      double age,
      String next_checkup,
      List vaccinations,
      bool sold,
      String image_url}) {
    return Pet(
        id: id ?? this.id,
        name: name ?? this.name,
        breed: breed ?? this.breed,
        colour: colour ?? this.colour,
        age: age ?? this.age,
        next_checkup: next_checkup ?? this.next_checkup,
        vaccinations: vaccinations ?? this.vaccinations,
        sold: sold ?? this.sold,
        image_url: image_url ?? this.image_url);
  }

  Pet.fromJson(Map json)
      : id = json["id"],
        name = json["name"],
        breed = json["breed"],
        colour = json["colour"],
        age = json["age"],
        next_checkup = json["next_checkup"],
        vaccinations = json["vaccinations"],
        sold = json["sold"],
        image_url = json["image_url"];

  Map toJson() => {
        "id": id,
        "name": name,
        "breed": breed,
        "colour": colour,
        "age": age,
        "next_checkup": next_checkup,
        "vaccinations": vaccinations,
        "sold": sold,
        "image_url": image_url,
      };

  @override
  String toString() {
    return toJson().toString();
  }
}
