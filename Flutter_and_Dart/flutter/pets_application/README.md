# pets_application

A new Flutter application.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

This Apllication is mainly for pet store.


Here they can add , edit and view pet details 

The backend code can be cloned from the same repository from Week3/Day4/exercise1 folder 

change the ApiUrl in the config file before running the app 


To run the App 

flutter run 
