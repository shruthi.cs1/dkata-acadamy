
void main(){

 //Exception Handling 
  

  
  try{
    double result = 12 / 0; 
    print ("The result is $result");
  }on IntegerDivisionByZeroException {
    print("Divided by zero is not allowed");
    
  }
  
  // catch 
  
  try{
    double result1 = 12 / 0; 
    print ("The result is $result1");
  }catch(e,s) {
    print("Exception is $e");
    // Stack trace
    print("stack trace is \n $s");
    
  }

  
   // finally 
  
  try{
    double result1 = 12 / 0; 
    print ("The result is $result1");
  }catch(e,s) {
    print("Exception is $e");
    // Stack trace
    print("stack trace is \n $s");
    
  }finally{
    print("i am always there!!");
  }
  
 
  
}