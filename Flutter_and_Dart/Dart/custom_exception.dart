
void main(){

// Custom exception 
  try{
    depositMoney(0);
  } catch(e){
    print(e.errorMessage());
  }finally{
    print("i am always there!!");
  }
  
  
}

class customException implements Exception {
  
  String errorMessage(){
    return "You cannot enter amount less than 0";
  }
}

depositMoney(amount){
  if(amount <= 0){
     throw new customException();  
  }
}