

void main(){

// define class and object
  var shruthi = new Trainer("shruthi","cs",27,"female","India, Bangalore","trainer","Dancing");
  var radha = new Trainer("radha","reddy",37,"male","India, Bangalore","trainer","Talking");
  var shanon = new Trainee("shanon","s",17,"female","Indo, jakarta","traniee","Reading");

  shruthi.greeting();
  radha.greeting();
  shanon.greeting();
  

  
}

// Property and behavior 
class Person {
  
     String first;
     String last ;
     int age ;
     String gender;
     String address;
     String type;
  
  
   Person(String first,String last,int age,String gender,String address,String type){
     this.first = first;
     this.last = last;
     this.age = age;
     this.gender = gender;
     this.address = address;
     this.type = type;
   }
   
   String greeting(){
     
         print("Hi i am ${this.first} ${this.last}");
           
        }
  
  
}

class Trainee extends Person{
  
  String hobby;
  
  Trainee(String first,String last,int age,String gender,String address,String type,String hobby) : super(first,last,age,gender,address,type){
    
     this.hobby = hobby;
  }
  
  // method overriding
  
  String greeting(){
         super.greeting();
         print("my hobby is ${this.hobby}");
        }
}
class Trainer extends Person{
  
  String skill;
  
  Trainer(String first,String last,int age,String gender,String address,String type,String skill) :         super(first,last,age,gender,address,type){
     this.skill = skill;
  }
  
  // method overriding
  
  String greeting(){
         super.greeting();
         print("my skill is ${this.skill}");
        }
}