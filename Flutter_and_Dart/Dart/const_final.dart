void main(){
  // Final Keyword 
  
  final cityName = "Bangalore";
  
  // const keyword
  
  const pi =  3.14;
  
     
}

class circle {
  final color =  "red";
    
  // Only static fields can be declared as const
  static const pi =  3.14;
}