
void main(){

// define class and object
  var shruthi = new Person("shruthi","cs",27,"female","India, Bangalore","trainer");
  var radha = new Person("radha","reddy",37,"male","India, Bangalore","trainer");
  shruthi.greeting();
  radha.greeting();
  var custom =  new Person.customConstructor();
  var customradha = new Person.anotherConstructor("shanon","s",37,"male","Indo, jakarta","trainee");
  customradha.greeting();

  
}

// Property and behavior 
class Person {
  
     String first;
     String last ;
     int age ;
     String gender;
     String address;
     String type;
  
//   Person(){
//     print("This is the default constructor");
//   }
  
  // Parameterized contructor
  
   Person(String first,String last,int age,String gender,String address,String type){
     this.first = first;
     this.last = last;
     this.age = age;
     this.gender = gender;
     this.address = address;
     this.type = type;
   }
  
  // Custom Custom Constructor
  
  Person.customConstructor(){
    print("This is my custom contructor");
  }
  
  // Another named constructor 
  
  Person.anotherConstructor(String first,String last,int age,String gender,String address,String type){
    this.first = first;
     this.last = last;
     this.age = age;
     this.gender = gender;
     this.address = address;
     this.type = type;
    
  }
  
  
  
   
   String greeting(){
     
         print("Hi i am ${this.first} ${this.last}");
           
        }
  
  
}