void main(){
  
List<int> numberList = List(5); //List -> fixed length
  numberList[0] = 1;
  numberList[1] = 2;
  numberList[2] = 3;
  numberList[3] = 4;
  numberList[4] = 5;
  
  // Differernt ways to print all numberlist elements 
  
  for(var eachelement in numberList){
    print(eachelement);
    
  }
  
  // Using lamda 
  
  numberList.forEach((eacheleement) => print(eacheleement));
  
  //Using index 
  
  for(var i=0;i<5;i++){
    print(numberList[i]);
    
  }
  
 // Which is applicable only on growable list
  
  List<int> growableList = List();
  growableList.add(56);
  growableList.add(57);
  growableList.add(58);
  growableList.add(50);
  
   print(growableList);
  
  growableList.remove(56);
  
  growableList.removeAt(2);
  
   print(growableList);
  
  growableList.clear();
  
  print(growableList);
  
 
 
}