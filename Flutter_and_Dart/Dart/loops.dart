void main(){
 //Just to print hello n times 
  
  for(var i=0;i<4;i++){
    print("Hello");
    
  }
  
   var j = 0;
  while(j < 4){
     print("Hello in while");
     j = j + 1 ;
    
  }
  
  var z = 0;
  
  do{
    print("hello in do while");
    z++;
  }while(z < 4);
  
  
  // display all even numbers within 10
  
  for(var number = 1; number <= 10 ; number++ ){
    if(number%2 == 0){
      print("$number is a even number");
      
    }
  }
  
  // for in Loop 
  
  List mobiles = ["IPhone","Moto","Samsaung"];
  for(String eachmobile in mobiles){
    print ("$eachmobile");
    
  }
  
  // Use whiile loop to print even numbers 
  
  var eachnumber = 1;
  
  while(eachnumber < 10){
    
    if(eachnumber % 2 == 0){
        print ("$eachnumber");      
    }
    eachnumber++;
  }
  
  // Do While to print even numbers 
  
  var evenNumber = 1;
  do{
    if(evenNumber % 2 == 0){
      print("$evenNumber");
      
    }
    evenNumber++;
  }while(evenNumber < 10);
  
  //print only 1 to 5 but do not change the condition is using break 
  
  for(var i=0;i<10;i++){
    print("$i");
    if(i == 5){
      break;
    }
    
  }
  
  //nestead Loop with breaks and label 
  
  outterloop : for(var i=0;i<=3;i++){
    for(var j=0;j<=3;j++){
      print("$i:$j");
      if(j==2 || i==2){
        break outterloop;
      }
      
      
    }
  }
  
  // continue keyword : print 1 to 10 and skip 5 
  
  for(var i=0;i<10;i++){
    
    if(i == 5){
      continue;
    }
    print("$i");
    
  }
 
}