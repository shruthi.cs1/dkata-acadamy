
void main(){
  // If Sstatement 
  
  int salary = 250000;
  if(salary >= 25000000){
    print ("Wowwww!! Good going ");
    
  }else{
    print("Please work hard !!");
  }
  
 // If else If statement 
  
  int marks = -100;
  
  if(marks > 70 && marks <= 100 ){
    print ("A+ grade");
  }else if(marks > 60 && marks <= 70){
    print ("A grade");
  }else if(marks > 50 && marks <= 60){
    print ("B+ grade");
  }else if(marks > 40 && marks <= 50){
    print ("B grade");
  }else if(marks > 30 && marks <= 40){
    print ("c+ grade");
  }else if(marks <= 30 && marks >= 0){
    print ("Failed");
  }else{
    print ("Invalid Marks");
  }
  
  
  // conditional expression 
  
  int a = 2;
  int b = 3;
  
  a < b ? print("$a is smaller") : print("$b is smaller") ;
  
  // Assign the output 
  
  var smallerNumber = a < b ? a : b ; 
  
  print ("$smallerNumber is smaller");
  
  // conditional expression type 2 which checks only null 
  
  String name = "Shruthi";
  String nameToPrint = name ?? "Hi Guest User";
  
  String fullname = null;
  String fullnametoPrint = fullname ?? "Hi, name is null";
  
  int score = 0;
  int scoreValueToPrint = score ?? 100;
  
  print(nameToPrint);
  print(fullnametoPrint);
  print(scoreValueToPrint);
  
  
  // Switch case 
  
  var grade = 'A';
  
  switch(grade){
    case 'A':
      print("Very good");
      break;
    case 'B':
      print("Good , Room to improve");
      break;
    case 'C':
      print("Work Hard");
      break;
    case 'F':
      print ("You have failed");
      break;
    default:
      print ("Invalid grade"); 
  }
}