void main(){
   // Literals  -  Simple example for literals 
  
    var isCool = true;
    var x = 2;
    var name = "Shruthi";
    var score =  4.5;
  
    // Various ways to provide string literals 
  
   String s1 = 'string1';
   String s2 = "string2";
   String s3 = 'It\'s easy';
   String s4 = "It's easy";
   String s5 = 'this is a very big statement'
                'another string ';
  
  // String Interpolation 
  
  String firstName = "Shruthi";
  String greeting = "My name is $firstName";
  
  
  //printing out the strings
  
  print(greeting);
  print ("The number of character in string ${name.length}");
  
  // Same with numbers 
  
  int l = 10;
  int b = 20 ; 
  
  print ("Sum of l and b is ${l + b}");
  print ("Area of reactangle is ${ l * b }");
      
      
}