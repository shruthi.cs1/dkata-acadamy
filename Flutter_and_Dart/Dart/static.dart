void main(){
  
  var circle = new Circle();
  //print(circle.pi); // is not allowed
  
  print(Circle.pi);
  Circle.calculate();
  circle.normalFunction();


  

  
}

class Circle {
  
  static const double pi = 3.14; // making it global for all instance and cant change the value of pi coz of const
  
  int maxRadius = 4;
  
  static calculate(){
    print("i am in function");
    //normalFunction() // is not allowed 
    //this.maxRadius // is also not allowed
  }
  
  void normalFunction(){
    
    Circle.calculate();
    
  }
}