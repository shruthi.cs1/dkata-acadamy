void main(){
  
 Function addtwonumbers = (int a , int b) => print(a + b);
  
 someOtherFuntion("Hello",addtwonumbers);
  
  var task = taskToPerform();
  task(4);
  
 


  

  
}

// Funtion as a argument 

void someOtherFuntion(String text,Function myFuntion){
  print("${text}");
  myFuntion(2,4);
  
}

// Funtion returns function 

Function taskToPerform () {
  Function multiplyByFour = (int number) => print(number * 4);
  return multiplyByFour;
}